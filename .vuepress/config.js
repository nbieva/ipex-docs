module.exports = {
    title: 'IPEX docs',
    description: 'Documentation on deliveries',
    head: [
        ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js' }],
        ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js' }],
        ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css' }],
        ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i,900,900i&display=swap' }],
        ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' }],
        ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css2?family=Atkinson+Hyperlegible:ital,wght@0,400;0,700;1,400;1,700&display=swap' }],
        ['link', { rel: 'stylesheet', type: 'text/css', href: '/assets/styles.css' }]
    ],
    markdown: {
        lineNumbers: true
    },
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            { text: 'IPEX V2', link: 'http://ipex.eu' },
            { text: 'Bitbucket repo', link: 'https://bitbucket.org/nbieva/ipex-html/src/master/' },
            { text: 'Former Test site', link: 'https://ipextest.netlify.com' },
            { text: 'IPEX.eu', link: 'https://ipex.eu' }
        ],
        sidebarDepth: 0,
        sidebar: [
            {
                title: 'IPEX Accessibility',
                collapsable: false,
                children: [
                    'accessibility/introduction',
                    'accessibility/templates',
                    'accessibility/issues',
                    'accessibility/keyboard',
                    'accessibility/list-of-changes',
                    'accessibility/tools',
                    'accessibility/glossary'
                ]
            },
            {
                title: 'IPEX Search',
                collapsable: false,
                children: [
                    'search/main-issues',
                    'search/merging',
                    'search/interface-elements',
                    'search/sidebar',
                    'search/filters',
                    'search/results-calendar',
                    'search/results-documents',
                    'search/results-parliaments',
                    'search/results-news',
                    'search/results-conferences',
                    'search/results-own'
                ]
            },
            {
                title: 'IPEX Files',
                collapsable: false,
                children: [
                    'files/files',
                    'files/components'
                ]
            },
            {
                title: 'Back-office CMS',
                collapsable: false,
                children: [
                    'cms/static-pages'
                ]
            },
            {
                title: 'General documentation',
                collapsable: false,
                children: [
                    'documentation/new-ipx-tags',
                    'documentation/empty-home-carousels',
                    'documentation/logo-in-parliament-details',
                    'documentation/conferences-themes',
                    'documentation/image-meeting',
                    'search/filter-year',
                    'deadlines/deadlines',
                    'documentation/login',
                    'documentation/new-ipex-logo',
                    'documentation/np-materials',
                ]
            },
            {
                title: 'Ant Design customs',
                collapsable: false,
                children: [
                    'ant-design-customs/selects',
                    'ant-design-customs/carousel'
                ]
            },
            {
                title: 'New contents',
                collapsable: false,
                children: [
                    'newcontents/newcontent-creation',
                    'newcontents/screens',
                    'newcontents/ncmodule',
                    'newcontents/newcontent',
                    'newcontents/nccard'
                ]
            },
            {
                title: 'Social sharing module',
                collapsable: true,
                children: [
                    'documentation/social',
                    'documentation/social-dev'
                ]
            },
            {
                title: 'CMS',
                collapsable: false,
                children: [
                    'cms/editor',
                    'documentation/edit-icon'
                ]
            }
            

        ]
    }
}