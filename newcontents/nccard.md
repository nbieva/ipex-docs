---
title: New content card
lang: fr-FR
---

# New content card

<p class="lead">New Content cards are displayed among other IPEX elements in the advanced search results page. There is no specific search tool to browse New Contents but they may be included in search results for other sections.</p>

## In search results

The New content card links directly to the New Content details page.

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/advanced-search/results/">Demo</a>

<a data-fancybox="newcontents" title="" href="/assets/nc-screen-03.png">![](/assets/nc-screen-03.png)</a>

## HTML markup

```html
<div class="ipx-card result-item newcontent-card">
    <a href="/" class="ipx-card-content">
        <h3 class="ipx-card-title">New content title</h3>
        <p class="ipx-card-description">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi omnis porro voluptates, provident ullam impedit fugit dolorum voluptatum eaque ab ad temporibus possimus delectus! Eum velit accusamus totam perferendis vero.
        </p>
        <div class="ipx-card-footer d-flex flex-column flex-grow-1">
            <small class="d-flex">
                <span class="link-icon material-icons mr-2">link</span>
                <span class="font-weight-bold mr-2">01/01/2001</span>
                <span>Source: Hellenic Parliament</span>
            </small>
            <small class="d-flex">
                <span>Linked to: german Bundestag</span>
            </small>
        </div>
    </a>
</div>
```