---
title: New content module
lang: fr-FR
---

# New content module

<p class="lead">The new content module is displayed directly before the IPEX footer. There are<strong> two different versions</strong> of this. One for only one item, and another when there is more than one item. If number of items exceeds 5 or 10 (to be defined), we add a <em>Load more</em> button.</p>

## With one item

This version shows a bit more information than the second one. However, there are no links or actions on the badges.

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/newcontentindication/">Demo</a>

<a data-fancybox="newcontents" title="" href="/assets/nc-01.png">![](/assets/nc-01.png)</a>

```html
<!-- ////////////////////////////////////
WITH ONE NEW CONTENT 
//////////////////////////////////// -->

<section class="newcontentbox newcontentbox-bottom">
    <div class="container-regular d-flex pb-4">
        <span class="link-icon material-icons mr-2">link</span>
        <div class="nc-item">
            <div>
                <div class="nc-title">
                    <h3 class="nc-title d-flex align-items-center">
                        This is another new content title
                    </h3>
                    <div class="newcontent-metas">
                        <p class="produced d-flex flex-column flex-md-row  align-items-start align-items-md-center">
                            <strong>Produced by:</strong>Chambre des Représentants</p>
                    </div>
                </div>
                <div class="nc-summary">
                    <p>New content description lorem ipsum dolor, sit amet consectetur adipisicing elit. Atque,
                        laboriosam exercitationem quidem error illum magni eaque consequuntur nesciunt perferendis harum
                        id aperiam iusto cumque, quibusdam, iste eveniet? Nesciunt, culpa sapiente?</p>
                    <div class="newcontent-metas">
                        <div class="files d-flex align-items-center">
                            <span class="badge mr-2">3</span> files available
                        </div>
                        <div class="author d-flex align-items-center">
                            <span class="mr-2">Author:</span>
                            <span class="badge">John Doe-Smith</span>
                        </div>
                        <div class="produced d-flex align-items-center">
                            <span class="mr-2">Content produced by:</span>
                            <span class="badge">Sénat</span>
                        </div>
                        <div class="keywords d-flex align-items-center">
                            <span class="mr-2">Content associated with:</span>
                            <span class="badge mr-2">Brexit</span>
                            <span class="badge mr-2">EU Affairs</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nc-link">
                <a href="/newcontent" title="View new content"
                    class="nc-open-icon material-icons md-36">keyboard_arrow_right</a>
            </div>
        </div>
    </div>
</section>
```

## With lots of items

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/newcontentmodule/">Demo</a>

<a data-fancybox="newcontents" title="" href="/assets/nc-02.png">![](/assets/nc-02.png)</a>

```html
<!-- ////////////////////////////////////
WITH MORE THAN ONE NEW CONTENT 
//////////////////////////////////// -->

<section class="newcontentbox newcontentbox-bottom">
    <div class="container-regular d-flex pb-4">
        <div class="nc-list">
            <span class="link-icon material-icons mr-2">link</span>
            <div class="nc-count">
                <p><strong>8</strong> New Contents associated</p>
            </div>
            <div class="nc-item">
                <div>
                    <div class="nc-title">
                        <h3 class="nc-title d-flex align-items-center">
                            New content title laboriosam exercitationem quidem error illum magni eaque
                        </h3>
                        <div class="newcontent-metas">
                            <p class="produced d-flex flex-column flex-md-row align-items-start align-items-md-center">
                                <strong>Produced by:</strong>Chambre des Représentants</p>
                        </div>
                    </div>
                </div>
                <div class="nc-link">
                    <a href="/newcontent" title="View new content"
                        class="nc-open-icon material-icons md-36">keyboard_arrow_right</a>
                </div>
            </div>
            <div class="nc-item">
                <div>
                    <div class="nc-title">
                        <h3 class="nc-title d-flex align-items-center">
                            This is another new content title
                        </h3>
                        <div class="newcontent-metas">
                            <p class="produced d-flex flex-column flex-md-row  align-items-start align-items-md-center">
                                <strong>Produced by:</strong>Chambre des Représentants</p>
                        </div>
                    </div>
                </div>
                <div class="nc-link">
                    <a href="/newcontent" title="View new content"
                        class="nc-open-icon material-icons md-36">keyboard_arrow_right</a>
                </div>
            </div>
            <div class="nc-item">
                <div>
                    <div class="nc-title">
                        <h3 class="nc-title d-flex align-items-center">
                            And this one is, again, another one but with a longer title
                        </h3>
                        <div class="newcontent-metas">
                            <p class="produced d-flex flex-column flex-md-row align-items-start align-items-md-center">
                                <strong>Produced by:</strong>Chambre des Représentants</p>
                        </div>
                    </div>
                </div>
                <div class="nc-link">
                    <a href="/newcontent" title="View new content"
                        class="nc-open-icon material-icons md-36">keyboard_arrow_right</a>
                </div>
            </div>
            <div class="nc-item">
                <div>
                    <div class="nc-title">
                        <h3 class="nc-title d-flex align-items-center">
                            Another one
                        </h3>
                        <div class="newcontent-metas">
                            <p class="produced d-flex flex-column flex-md-row align-items-start align-items-md-center">
                                <strong>Produced by:</strong>Chambre des Représentants</p>
                        </div>
                    </div>
                </div>
                <div class="nc-link">
                    <a href="/newcontent" title="View new content"
                        class="nc-open-icon material-icons md-36">keyboard_arrow_right</a>
                </div>
            </div>
            <div class="nc-item">
                <div>
                    <div class="nc-title">
                        <h3 class="nc-title d-flex align-items-center">
                            And a very long title. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Molestiae
                            veniam, quae quaerat ducimus sequi quidem tempora id sint illum obcaecati fuga ut iure
                            repellendus dolor officiis corrupti, ex est vitae.
                        </h3>
                        <div class="newcontent-metas">
                            <p class="produced d-flex flex-column flex-md-row align-items-start align-items-md-center">
                                <strong>Produced by:</strong>Chambre des Représentants</p>
                        </div>
                    </div>
                </div>
                <div class="nc-link">
                    <a href="/newcontent" title="View new content"
                        class="nc-open-icon material-icons md-36">keyboard_arrow_right</a>
                </div>
            </div>
            <!-- Insert Load More button if more than 5 or 10 nc-items -->
            <div class="text-center">
                <button class="btn btn-load">
                    Load more
                </button>
            </div>
        </div>
    </div>
</section>
```