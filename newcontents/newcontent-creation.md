---
title: New contents (September update)
lang: fr-FR
---

# New contents (September update)

One institution (or more than one) creates (or co-create) a *New Content*.

That *New Content* has:

+ A **title**
+ One or more than one **source Institution(s)**
+ **Keywords** (Board keywords)
+ One **content** (Description / Rich text editor)
+ **Documents** (Attached files)
+ **Classification** (Eurovoc, Subject matters)
+ **Linked pages**

The *New Content* **can be linked to any content page** in IPEX (News, Calendar event, Document.. or another *New Content*)

The *New Content* has its own page where its data is displayed (see list above).

This is how a *New Content* detail page looks like:

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/newcontent/">Demo</a>

<a data-fancybox="newcontents" title="" href="/assets/nnc-10.png">![](/assets/nnc-10.png)</a>

## The New Content Module

The New Content Module appears automatically when a new content is linked to a page. It can be added to any page in IPEX.

<a data-fancybox="newcontents" title="" href="/assets/nnc-news-00.jpg">![](/assets/nnc-news-00.jpg)</a>

On hover, the box is highlighted and an arrow indicates the link. On click (entire box), the *New Content* detail page is open.
It displays **maximum 5 items** when the page is loaded. If there are more items to load, it can be done using the **Load More button**.

<a data-fancybox="newcontents" title="" href="/assets/nnc-news-more.jpg">![](/assets/nnc-news-more.jpg)</a>

## How and where files are displayed

When an Institution creates a* New Content*, some files can be attached to it. **When a file is attached to a *New Content*, it appears automatically in the Public Files section of the source Institution(s)**. 

If the NC is co-created by more than one Institution, the file appears on every Parliament's page that co-created the NC.

**On the Public Files section, *New Contents* files are grouped** below the New Content title to identify the context in which they were created.

When clicking on the purple link icon, the New Content detail page is open.
When clicking on the file title, the file is downloaded.

<a data-fancybox="newcontents" title="" href="/assets/nnc-files-01.jpg">![](/assets/nnc-files-01.jpg)</a>
<a data-fancybox="newcontents" title="" href="/assets/nnc-files-00.jpg">![](/assets/nnc-files-00.jpg)</a>