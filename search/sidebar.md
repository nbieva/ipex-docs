---
title: Edit search with filters
lang: fr-FR
---

# Editing search only with sidebar filters

## One (and only one) place

It is necessary for the users to clearly **identify one place to edit their search**. 
That place should be the sidebar filters.

If the user wants to make a new search, he/she can use the IPEX Search Home Screen, where he/she started at first, the Legislative Database page, or the Navigation Search Module.

## Removing the Edit Search form (sliding drawer)

The Edit Search Form drawer should be removed from the interface.
Keeping it would be very confusing and frustrating for the user, as this does not edit the search, but triggers a new one, removing/resetting all sidebar filters.

We should then be sure that each field in that form has its ‘sidebar filter version’. (Dates, codes, search term, scrutiny events, etc..). Most of them already have but there are some inconsistencies. (See the attached “List of filters” spreadsheet)
