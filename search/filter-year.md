---
title: Filter Year
lang: fr-FR
---

# Filter Year

<p class="lead">The filter year is for the results page of the Calendar Advanced Search.</p>

<a data-fancybox="year" title="" href="/assets/year-01.jpg">![](/assets/year-01.jpg)</a>

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/advanced-search/results-calendar">Demo</a>

## HTML markup

To be included IN the collapse box of the year filter. 
anchors may be replaced by buttons if you want, as styles target the <code>.year-item</code> class.
Links hrefs, value and attributes **should of course be replaced** by dynamic content. 

The filter input is an ant-design component. So do not copy-paste this particular code. It will be different in Angular.

Each time a year is selected, a new search is triggered.

Add a **.disabled** class on selected items.

```html
<a-input-search placeholder="Filter years" style="width: 100%" class="filter-input" @search="onSearch" />
<div class="years mt-3">
    <button id="#" class="year-item" title="2010 (27 results)">2010<span>27</span></button>
    <button id="#" class="year-item" title="2011 (19 results)">2011<span>19</span></button>
    <button id="#" class="year-item" title="2012 (22 results)">2012<span>22</span></button>
    <button id="#" class="year-item disabled" title="2013 (101 results)">2013<span>101</span></button>
    <button id="#" class="year-item" title="2014 (66 results)">2014<span>66</span></button>
    <button id="#" class="year-item" title="2015 (50 results)">2015<span>50</span></button>
    <button id="#" class="year-item" title="2016 (17 results)">2016<span>17</span></button>
    <button id="#" class="year-item" title="2017 (35 results)">2017<span>35</span></button>
    <button id="#" class="year-item disabled" title="2018 (255 results)">2018<span>255</span></button>
    <button id="#" class="year-item" title="2019 (7 results)">2019<span>7</span></button>
    <button id="#" class="year-item" title="2020 (96 results)">2020<span>96</span></button>
</div>
```

## Less

The following styles are located in the **search.less** file.

```less
.years {
  display:flex;
  flex-wrap:wrap;
  .year-item {
    display:flex;
    align-items:center;
    justify-content: space-between;
    padding:.2rem .25rem .2rem .7rem ;
    margin:.3rem;
    font-size:.9rem;
    color:#111;
    border-radius:5rem;
    background:#f5f5f5;
    transition:all ease .2s;
    span {
      display:flex;
      align-items:center;
      justify-content: center;
      min-width:24px;
      padding:.1rem .2rem;
      margin-left:.4rem;
      font-size:.8rem;
      font-weight:600;
      border-radius:5rem;
      background:white;
    }
    &:hover {
      color:@baseflash;
      background:darken(#f5f5f5,3%);
      text-decoration:none;
      cursor:pointer;
    }
    &.disabled {
      color:@lightgray;
      background:@gray4;
      &:hover {
        cursor:default;
      }
    }
  }
}
```