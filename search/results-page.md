---
title: Results page
lang: fr-FR
---

# The results page

## Elements of the page

As from now, we should have a unique template/interface for the results of an IPEX search.
The results page should display:

1.	The number of results found
2.	The section in which the search has been performed (if applicable)
3.	The main search input for search terms
4.	The Print button
5.	The Save search (or edit saved search) button - If logged in.
6.	The saved search status (indicates whether or not the user already saved his/her search, and if modifications have been made since then)
7.	The currently selected filters (as badges) – this may be removed, as the selected items would be marked as “checked” in the sidebar filters (to be discussed)
8.	The sidebar filters
9.	The list of results

## The main search input (for search terms)

The main search input is where the users can enter specific search terms.
Each time the user add keyword and triggers a search from there, the search is performed taking into consideration the actual selected filters if there are some.

To trigger a brand new search, users can use one of the 3 entry points for the search, described in Point 4 of this document (Legislative database, Search landing page and Search navigation module).