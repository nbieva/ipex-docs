---
title: Results - Parliaments
lang: fr-FR
---

# Parliaments

## Labels

+ Section label: **Parliaments**
+ Result item label: **Parliament file**
+ Action on click: **Download**

## Filters + their respective type

All filter items are associated with a corresponding number of results that would be returned if selected.

### For Parliaments files

+ **Search term(s)** (Using the main search input on results page)
+ **Document type** (category) : Regular list + Dynamic filtering input. Is it "Board keywords"?
+ **Published in** : Badges with count for each 
+ **Institutions** : Regular list with multiple checkboxes select + Dynamic filtering input
+ **Board keywords** : (If applicable - To be confirmed) Regular list with multiple checkboxes select + Dynamic filtering input

## Results

The Parliament files result cards display the following information:

1.	File **title**
2.	File **type** (PDF, DOC..)
3.	File **language** (EN, FR..)
4.	File **size** (282Kb, 32Mb..)
5.	File **date**
6.	**Country** of the Chamber
7.	**Chamber**
8.	Expandable section with **search term hits** (if search term was used)
9.	Result **type** (PARLIAMENT FILE)