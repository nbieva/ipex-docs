---
title: Results - Calendar
lang: fr-FR
---

# Calendar

## Labels

+ Section label: **Calendar**
+ Result item label: **Event**

## Filters + their respective type

All filters items are associated with corresponding number of results that would be returned if selected.

+ **Search term(s)** (Using the main search input on results page)
+ **Address** : Regular list + Dynamic filtering input
+ **Event type** : Regular list + Dynamic filtering input
+ **Year** : Badges with count for each - Note: Date range in the Calendar Search form.
+ **Institutions** : Regular list with multiple checkboxes select + Dynamic filtering input
+ **Board keywords** : (If applicable - To be confirmed) Regular list with multiple checkboxes select + Dynamic filtering input

## Results

The Event result card displays the following information:

1.	**Event title**
2.	**Event date(s)**
3.	**Indicative dates** badge if necessary
4.	**Place** (Address)
5.	Expandable section with **search term hits** (if search term was used)
6.	Result **type** (EVENT)
