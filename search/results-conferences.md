---
title: Results - Conferences
lang: fr-FR
---

# Conferences

## Labels

+ Section label: **Conferences**
+ Result item label: **Conference file**
+ Action on click: **Download**

## Filters + their respective type

All filter items are associated with a corresponding number of results that would be returned if selected.

**For the moment, as filters, there is just "Year".**

### For Conferences files

+ **Search term(s)** (Using the main search input on results page)
+ **Document type** (category) : Regular list + Dynamic filtering input. Is it "Board keywords"?
+ **Published in** : Badges with count for each 
+ **Conference** (EU Speakers, COSAC, ..): Regular list with multiple checkboxes select
+ **Board keywords** : (If applicable - To be confirmed) Regular list with multiple checkboxes select + Dynamic filtering input

## Results

The Conference files result cards display the following information:

1.	File **title**
2.	File **type** (PDF, DOC..)
3.	File **language** (EN, FR..)
4.	File **size** (282Kb, 32Mb..)
5.	File **date**
6. **Conference**
7.	**Meeting** 
8.	**Event**
9. File **category** (or keywords, or type..) - Is it "Board keywords"?
10.	Expandable section with **search term hits** (if search term was used)
11.	Result **type** (CONFERENCE FILE)