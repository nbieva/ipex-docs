---
title: Merging Searches
lang: fr-FR
---

# Merging Searches

Merging General Search and Advanced Search

## Three entry points for the search :

+ The **IPEX Search Home Screen** with different search forms (for all, documents, news, etc..)
+ The **Legislative Database** page with the Documents search form
+ The **Navigation Search Module**

## Two different cases:

+ The **user searches in all sections** (documents, news, events..)  with a keyword (or not), and gets the results with only one filter: the Type filter. Once the user chooses one particular type, he/she will get results for that type with all the filters for that particular type. 
This means building a bridge between what was the “General search” to what was the “Advanced search”. 
We should then consider the IPEX search as a unique interface.
+ The **user searches in a specific section** (documents, news, events..) and gets results with all the filters for that particular type. (This applies also to the Legislative Database page or the Navigation Search Module if the user uses section icons to trigger the search). 
This is the actual “Advanced search”.

**Important** : For technical limitations, it is not possible to implement, for the moment, a search on two different types of results at the same time (For example Documents and News). 