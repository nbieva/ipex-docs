---
title: Filters
lang: en-EN
---

# Filters

For the moment, some filters are not yet implemented and others are not present in the section Search form. So the idea here is to provide a complete list of filters, and possibly identify which ones could be missing.

Also, the goal is to align as much as possible search filters and search forms, so we can, if decided, remove the sliding drawer from the interface in a near future and edit our searches only using the filters.

## Checkboxes

Using checkboxes, users will have the ability to add/remove/combine filters to build the Search they need. 

<a data-fancybox="" title="" href="/assets/filter-status.jpg">![](/assets/filter-status.jpg)</a>

For example:

+ The user wants to search for a scrutiny from year 2010 **AND** year 2015.
+ The user wants results with Reasoned opinion **AND** Political dialogue

The number of checked items will be displayed in the filter title, in order for that information to be accessible even if the filter is collapsed.

## How filters will work

1. Each filter item is available if there is **at least one result item available**.
2. Each filter box is available if there is **at least one filter item available**.
3. **Checkboxes are unchecked by default** (even if default value will be true). Once the user checks one item, its value will be set to true and other items will be set to false, as the users has indicate clearly their intention.
4. Long lists of items can have a filtering text input, maximum height and vertical scroll.

**Question** : Should search text inputs (for keyword search) have a “Limit text search to a specific language” capability, as in the Documents section form?

## The different identified filters types

1.	**Badges with count for each** (ex: Year) - Can by dynamically filtered in front.
2.	**Date range input** (if needed, to be discussed) May be necessary to align with the search forms.
3.	**Regular list with input** to dynamically filter items. For example when we have long list of items (A list of chambers or countries for example..)
4.	**Input with a pattern** (For example a Document reference..)
5.	**Regular list with checkboxes** in front of items. This will allow the user to activate or deactivate the item. 
6.	**Regular list with multiple checkboxes** in front of items. This will be used if we need more than a true/false filter (true/not true and/or false/not false). (See SCrutiny events filter)
7.	**Regular list with icons or specific visual elements**. For example for the scrutiny events (the coloured badges with letters) or the Scrutiny status (coloured ellipse)



<a data-fancybox="" title="" href="/assets/filters-list.jpg">![](/assets/filters-list.jpg)</a>

### 1. Badges with count for each

### 2. Date range input

### 3. Regular list with input to dynamically filter items

### 4. Input with a pattern 

### 5. Regular list with checkboxes

### 6. Regular list with multiple checkboxes

### 7. Regular list with Icons or specific visual elements