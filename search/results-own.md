---
title: Results - OWNs
lang: fr-FR
---

# OWNs

Note that the OWNs results described in here will be updated. Currently OWN pages are listed in the search results but as I understood, it will be only OWN files in the coming versions. I need more specific input in order to provide new mockups and changes in the front.
So this still has to be discussed and defined.

## Labels

+ Section label: **OWNs**
+ Result item label: **OWN file**
+ Action on click: **Download**

## Filters + their respective type

All filter items are associated with a corresponding number of results that would be returned if selected.

### For OWN files

+ **Search term(s)** (Using the main search input on results page)
+ **Document type** (category) : Regular list + Dynamic filtering input. Is it "Board keywords"?
+ **Published in** : Badges with count for each 
+ **Institutions** (Sources) : Regular list with multiple checkboxes select + Dynamic filtering input
+ **Board keywords** : (If applicable - To be confirmed) Regular list with multiple checkboxes select + Dynamic filtering input

## Results

The *OWN file* result cards display the following information:

1.	OWN **title**
2.	OWN **Description**
3.	Date of **publication**
4.	**Source** (Institution(s))
5.	File **date**
6.	**Linked IPEX pages**