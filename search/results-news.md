---
title: Results - News
lang: fr-FR
---

# News

## Labels

+ Section label: **News**
+ Result item label: **News**

## Filters + their respective type

All filter items are associated with a corresponding number of results that would be returned if selected.

+ **Search term(s)** (Using the main search input on results page)
+ **Published in** : Badges with count for each (Is it current Creation filter?)
+ **Institutions** (Source/Authors)  : Regular list with multiple checkboxes select + Dynamic filtering input
+ **Board keywords** : (If applicable - To be confirmed) Regular list with multiple checkboxes select + Dynamic filtering input

**Question** : Note that, for the moment, there is a “Creation date” filter. Maybe for a news, the published date would be more accurate?

## Results

The Event result card displays the following information:

7.	**News title**
8.	**Creation date**
9.	**Source(s)**
10.	Expandable section with **search term hits** (if search term was used)
11.	Result **type** (EVENT)