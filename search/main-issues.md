---
title: Introduction
lang: fr-FR
---

# Introduction

## Main current issues

### No bridge to the Advanced search

Having a General search and an Advanced search is confusing for the user, as we have no bridge from the first one to the second.

Once general results are filtered by Type, the user cannot go further and has absolutely no filter to use anymore. This is a dead end.
Solution is to have a bridge between General search and Advanced search, transparent for the user. (see below in this document)

### Edit search button is a New search action

The Edit search button does not edit the search. It opens a search form with prefilled inputs that triggers a new search.

If the user presses the search button, it triggers a new search and removes/erases all filters set by the user in the sidebar. 

For example, if a user chooses “Reasoned opinion” as a filter in the sidebar, he/she expects to find it checked in the “Edit search” form if he/she clicks on the ‘Edit search’ button. It is not the case. The two are not linked.

Search filters and Edit search form are in fact not aligned. Behind the scenes, they are not related, but for the user it is very confusing. 
They should be aligned, or the Edit search form should be removed. (see below in this document)

### Filters design

Most of the filters are not implemented with the right design. This should be corrected.

## Planning

### Upcoming

+ Setting a bridge between General Search and Advanced search
+ Analysing and implementing the complete list of filters for each result type with their correct corresponding designs (see further in this document)
+ Implementing checkbox capabilities in filters. This will allow users to select multiple values for a filter.
+ Unify interface (only one result page template)
  
### Following

+ Align filters and drawer
+ Remove drawer

## What is not in this documentation

### Back-end/Database impact information

The goal of this document is not to provide any technical analysis about IPEX Search and does not include impact on back-end/database or Elastic search, even if most of the following points were discussed with architect and developers.
It is a starting point, from the user point of view.

### Upcoming changes concerning the files in IPEX

It is still not clear how to integrate files in the search as, currently, only Parliament files and Conference files are indexed.
But we may need to find also Files attached to an event, a news (does not exist yet), a OWN, or even to a static page.

The main question is: 

Do we include those files as a sub-type of a section (like Scrutinies for documents) or do we create a specific section to search “Files” ?

This needs further information and is therefore not defined in the scope of this document.
