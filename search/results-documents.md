---
title: Results - Documents
lang: fr-FR
---

# Documents

## Labels

+ Section label: **Documents**
+ Result item label: **Document** or **Scrutiny**

## Filters + their respective type

All filter items are associated with a corresponding number of results that would be returned if selected.

### For Documents

+ **Search term(s)** (Using the main search input on results page)
+ **Type** (scrutiny or document) : List + icon
+ **Dossier code** : Regular list with count for each
+ **Year** : Badges with count for each
+ **Document Reference** : Input field (with pattern) + Button (to be discussed)

Note : the Dossier Code, Year, and Document number, present in the search form, could be under a “Document reference” filter, using a specific pattern.

+ **NP Threshold**: Regular list with multiple checkboxes select + visuals
+ **Authors**: Regular list with multiple checkboxes select + Dynamic filtering input
+ **Board keywords**: (If applicable - To be confirmed) Regular list with multiple checkboxes select + Dynamic filtering input

### For Scrutinies

+ **Scrutiny Year**: Badges or date range (Check if not duplicate with Deadline)
+ **Deadline** : Badges or date range
+ **Institutions** : Regular list with multiple checkboxes select + Dynamic filtering input
+ **Scrutiny status** (Not started, In progress, complete) : Regular list with multiple checkboxes select + visuals 
+ **Scrutiny events** : Multiple checkboxes with ipx-tags badges (OR)
+ **Transmitted by NP** (for scrutinies) : Badges or Date range

## Results

There are 2 different types of results for the document section search.

+ A **Document result card**
+ A **Scrutiny result card**

### For Documents

The Documents result card displays the following information:

1.	Document **reference**
2.	**Scrutiny events **badges (R,E,D,V,S..) + count
3.	Dossier **reference**
4.	Document **title**
5.	Expandable section with **search term hits** (if search term was used)
6.	Result **type** (DOCUMENT)

### For Scrutinies

The Scrutiny result card displays the following information:

1.	Document **reference**
2.	The **Chamber** (Institution)
3.	**Scrutiny events** badges (R,E,D,V,S..) + count
4.	Scrutiny **status**
5.	Expandable section with **search term hits** (if search term was used) - (not existing yet)
6.	Result **type** (SCRUTINY)