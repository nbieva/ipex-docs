---
title: New IPX TAGS
lang: fr-FR
---

# IPEX tags

<p class="lead">As requested by the business users, the ipex tags (shown on homepage, scrutinies, high activity doc, etc..) have to be updated to <strong>display the amount of "events" attached to a specific document/element</strong>. Of course this will only be displayed well when updating the LESS files (see sidebar). Ant Design tooltips should be add to this.</p>

 <video autoplay loop>
  <source src="/assets/tags1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video> 



<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/home/">Demo</a>

This concerns several places in IPEX. But the new number is not displayed on all. For example, on a scrutiny timeline, the tags are specific unique events. As there will be never more than one there, count is useless.

These are the main places where we find those badges:

+ Homepage: **Important issues**
+ Homepage: **High activity documents**
+ **High activity landing page** 
+ **Document: Header**
+ **Document: Scrutinies section**, on each chamber
+ **Scrutiny: Header** (just the title)
+ **Search results** cards
+ (Please let me know if you think something is missing here..)

Tags on Scrutiny timeline are left as they are, as they represent singular events.

All those elements have to be updated following the codes below.

For the moment, and to avoid impact on existing elements on IPEX, a new **ipx-title-wrapper** class has been created for document cards. See code below.

::: tip
Please note that the only way I can style and test this is through my test Nuxt application which is built on Vue.js. I used ant-design-vue to implement the components in the application before styling. I guess we may find some differences or inconsistencies when implementing this in the Angular application but it should mainly be ok. So please **double check the screenshots** that give you a **very precise idea of how it is supposed to be displayed.**
:::

### Use of Tooltip Ant Design component

We should generalize the use of the **Tooltip Ant Design component** on ipx-tags, but also in filling forms support (in advanced search for example) and other places.
Tooltips should be displayed on click in order not to disturb flow in the page.

<a data-fancybox="editor" title="" href="/assets/tags-02.png">![](/assets/tags-02.png)</a>

+ Documentation for Ant tooltips is here: [https://ant.design/components/tooltip/](https://ant.design/components/tooltip/)
+ The one I used in the nuxt project (ant-design-vue): [https://www.antdv.com/components/tooltip/](https://www.antdv.com/components/tooltip/)
+ You will find the code I used below:

#### Native HTML title attribute

To be used when there is no count.

```html
<span title="3 reasoned opinions have been sent" class="btn btn-opinion ipx-tag shadow-sm">
    R
    <span class="tag-count">3</span>
    <span class="sr-only">3 Reasoned opinions</span>
</span>
```

#### Ant Design tooltip

Same example with Ant Design tooltip (Vue.js implementation), use in home and results cards for example.

```html
<a-tooltip placement="top" overlayClassName="ipx-tooltip" trigger="click">
    <template slot="title">
    3 reasoned opinions have been sent
    </template>
    <span title="3 reasoned opinions have been sent" class="btn btn-opinion ipx-tag shadow-sm">
        <span class="tag-symbol">R</span>
        <span class="tag-count">3</span>
        <span class="sr-only">3 reasoned opinions have been sent</span>
    </span>
</a-tooltip>
```

### Structure for a carousel document card (Homepage)

This is an example for the structure of a card on the homepage.

<a data-fancybox="editor" title="" href="/assets/tags-01.png">![](/assets/tags-01.png)</a>

```html
<div class="ipx-card flex-column home-card home-high home-document">
    <a href="/" class="ipx-card-content w-100">
        <div class="ipx-title-wrapper">
            <div class="ipx-card-title">
                <h3>COM/2006/0147FIN</h3>
                <!-- if card has yellow flag -->
                <a-tooltip placement="top" overlayClassName="ipx-tooltip" trigger="click">
                    <template slot="title">
                    Yellow card
                    </template>
                    <span title="Yellow card" class="btn btn-yellowcard rounded ipx-tag shadow-sm">Y<span class="sr-only">Yellow card</span></span>
                </a-tooltip>
            </div>
            <div class="ipx-tags">
                <!-- If Reasoned opinion -->
                <a-tooltip placement="top" overlayClassName="ipx-tooltip" trigger="click">
                    <template slot="title">
                    3 reasoned opinions have been sent
                    </template>
                    <span title="3 reasoned opinions have been sent" class="btn btn-opinion ipx-tag shadow-sm">
                        <span class="tag-symbol">R</span>
                        <span class="tag-count">3</span>
                        <span class="sr-only">3 Reasoned opinions have been sent</span>
                    </span>
                </a-tooltip>
                <!-- If Political Dialog -->
                <a-tooltip placement="top" overlayClassName="ipx-tooltip" trigger="click">
                    <template slot="title">
                    <strong>1</strong> Political Dialogue
                    </template>
                    <span title="1 Political Dialogue" class="btn btn-dialog ipx-tag shadow-sm">
                        <span class="tag-symbol">D</span>
                        <span class="tag-count">1</span>
                        <span class="sr-only">1 Political Dialogue</span>
                    </span>
                </a-tooltip>
                <!-- If Information to exchange -->
                <a-tooltip placement="top" overlayClassName="ipx-tooltip" trigger="click">
                    <template slot="title">
                    <strong>1</strong> important information to exchange
                    </template>
                    <span title="There is 1 important information to exchange" class="btn btn-exchange ipx-tag shadow-sm">
                        <span class="tag-symbol">E</span>
                        <span class="tag-count">1</span>
                        <span class="sr-only">1 important information to exchange</span>
                    </span>
                </a-tooltip>
                <!-- If Opposition -->
                <a-tooltip placement="top" overlayClassName="ipx-tooltip" trigger="click">
                    <template slot="title">
                    A national Parliament makes known its opposition to bridging clauses
                    </template>
                    <span title="A national Parliament makes known its opposition to bridging clauses" class="btn btn-opposition ipx-tag shadow-sm">
                        <span class="tag-symbol">O</span>
                        <span class="tag-count">1</span>
                        <span class="sr-only">A national Parliament makes known its opposition to bridging clauses</span>
                    </span>
                </a-tooltip>
                <!-- If Subsidiarity -->
                <a-tooltip placement="top" overlayClassName="ipx-tooltip" trigger="click">
                    <template slot="title">
                    Subsidiarity procedure is in progress
                    </template>
                    <span title="Subsidiarity procedure is in progress and there are possible concerns." class="btn btn-subsidiarity ipx-tag shadow-sm">
                        <span class="tag-symbol">S</span>
                        <span class="tag-count">1</span>
                        <span class="sr-only">Subsidiarity procedure is in progress</span>
                    </span>
                </a-tooltip>
            </div>
        </div>
        <p class="ipx-card-description">
            Test title Proposal for a Council Regulation concerning the implementation of the Agreement concluded by the EC following negotiations in the framework of Article XXIV.6 of GATT 1994.
        </p>
    </a>
</div>
```

### Structure for a document card in search results or other lists (not carousels)

<a data-fancybox="editor" title="" href="/assets/tags-05.png">![](/assets/tags-05.png)</a>

```html
<div class="ipx-card result-item result-item-document">
    <a href="/" class="ipx-card-content w-100">
        <h3 class="ipx-card-title d-flex flex-column flex-sm-row">
            <span class="flex-shrink-0">COM/2013/0113FIN</span>
            <div class="ipx-tags d-flex justify-content-sm-end mt-2 mt-sm-0 ml-0 ml-sm-2">
                <a-tooltip placement="top" overlayClassName="ipx-tooltip" trigger="click">
                    <template slot="title">
                    <strong>3</strong> reasoned opinions have been sent
                    </template>
                    <span title="3 reasoned opinions have been sent" class="btn btn-opinion ipx-tag shadow-sm">
                        <span class="tag-symbol">R</span>
                        <span class="tag-count">3</span>
                        <span class="sr-only">3 Reasoned opinions have been sent</span>
                    </span>
                </a-tooltip>
            </div>
        </h3>
        <p class="ipx-card-description">
            Proposal for a Council <mark>Regulation</mark> concerning the implementation of the Agreement concluded by the EC following negotiations in the framework of Article XXIV.6 of GATT 1994, amending Annex I to <mark>Regulation</mark> (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff.
        </p>
        <div class="ipx-card-footer d-flex align-items-end flex-grow-1">
            <div class="ipx-card-type card-type-event mr-3">
                <img src="/icons/ipx-icon-documents-small.svg" class="type-icon"><small>Document</small>
            </div>
        </div>
    </a>
</div>
```

### Header of a document

Here is the tags for the header of a document. There is no tooltip here (as in High activity page), as the number of items is already displayed.

<a data-fancybox="editor" title="" href="/assets/tags-08.png">![](/assets/tags-08.png)</a>

```html
<div class="document-activity mr-5">
    <div>
        <span title="3 reasoned opinions have been sent" class="btn btn-opinion ipx-tag shadow-sm">
            <span class="tag-symbol">R</span>
            <span class="tag-count">3</span>
            <span class="sr-only">3 Reasoned opinions</span>
        </span>
        <span>
            3 reasoned opinions have been sent
        </span>
    </div>
    <div>
        <span title="Political Dialogue" class="btn btn-dialog ipx-tag shadow-sm">
            <span class="tag-symbol">D</span>
            <span class="tag-count">2</span>
            <span class="sr-only">2 Political Dialogues</span>
        </span>
        <span>
            2 political dialogues
        </span>
    </div>
    <div>
        <span title="There is important information to exchange" class="btn btn-exchange ipx-tag shadow-sm">
            <span class="tag-symbol">E</span>
            <span class="tag-count">1</span>
            <span class="sr-only">1 important information to exchange</span>
        </span>
        <span>
            1 important information to exchange
        </span>
    </div>
</div>
```

Same for High activity page (cards)

<a data-fancybox="editor" title="" href="/assets/tags-09.png">![](/assets/tags-09.png)</a>

### Tags in scrutiny cards (in a document)

Here is the tags for the scrutiny list in a document (scrutiny cards).

<a data-fancybox="editor" title="" href="/assets/tags-07.png">![](/assets/tags-07.png)</a>

```html
<div class="d-flex align-items-start">
    <span class="chamber flex-grow-1">{{item.name}}</span>
    <div class="d-flex mb-2 align-items-center justify-content-end flex-wrap">
        <a-tooltip  v-if="item.opinion" placement="top" overlayClassName="ipx-tooltip" trigger="click">
            <template slot="title">
            <strong>1</strong> Reasoned opinion has been sent
            </template>
            <span title="1 Reasoned opinion has been sent" class="btn btn-opinion ipx-tag shadow-sm">
                <span class="tag-symbol">R</span>
                <span class="tag-count">1</span>
                <span class="sr-only">1 Reasoned opinion</span>
            </span>
        </a-tooltip>
        <a-tooltip v-if="item.dialogue" placement="top" overlayClassName="ipx-tooltip" trigger="click">
            <template slot="title">
            <strong>1</strong> Political Dialogue
            </template>
            <span title="1 Political Dialogue" class="btn btn-dialog ipx-tag shadow-sm">
                <span class="tag-symbol">D</span>
                <span class="tag-count">1</span>
                <span class="sr-only">1 Political Dialogue</span>
            </span>
        </a-tooltip>
        <!-- If Information to exchange -->
        <a-tooltip v-if="item.exchange" placement="top" overlayClassName="ipx-tooltip" trigger="click">
            <template slot="title">
            <strong>1</strong> important information to exchange
            </template>
            <span title="There is 1 important information to exchange" class="btn btn-exchange ipx-tag shadow-sm">
                <span class="tag-symbol">E</span>
                <span class="tag-count">1</span>
                <span class="sr-only">1 important information to exchange</span>
            </span>
        </a-tooltip>
        <!-- If Opposition -->
        <a-tooltip v-if="item.opposition" placement="top" overlayClassName="ipx-tooltip" trigger="click">
            <template slot="title">
            A national Parliament makes known its opposition to bridging clauses
            </template>
            <span title="A national Parliament makes known its opposition to bridging clauses" class="btn btn-opposition ipx-tag shadow-sm">
                <span class="tag-symbol">O</span>
                <span class="tag-count">1</span>
                <span class="sr-only">A national Parliament makes known its opposition to bridging clauses</span>
            </span>
        </a-tooltip>
        <!-- If Subsidiarity -->
        <a-tooltip v-if="item.subsidiarity" placement="top" overlayClassName="ipx-tooltip" trigger="click">
            <template slot="title">
            Subsidiarity procedure is in progress
            </template>
            <span title="Subsidiarity procedure is in progress and there are possible concerns." class="btn btn-subsidiarity ipx-tag shadow-sm">
                <span class="tag-symbol">S</span>
                <span class="tag-count">1</span>
                <span class="sr-only">Subsidiarity procedure is in progress</span>
            </span>
        </a-tooltip>
    </div>
</div>
```

### Header of a Scrutiny

There is no tooltip info for the list of scrutiny events (bottom left) or the timeline, as they refer to unique events in time. We just add tags with count to the Scrutiny title (the one with the flag)

<a data-fancybox="editor" title="" href="/assets/tags-06.png">![](/assets/tags-06.png)</a>

```html
<div class="d-flex align-items-center mb-3">
    <div class="d-flex align-items-center flex-grow-1">
        <img class="flag flag-md mr-2 flag-es" src="/img/flags/es.svg" alt="">
        <h1 class="m-0">Cortes Generales</h1>
    </div>
    <div class="ipx-tags flex-grow-0">
        <a-tooltip placement="top" overlayClassName="ipx-tooltip" trigger="click">
            <template slot="title">
            <strong>1</strong> Reasoned opinion has been sent
            </template>
            <span title="1 Reasoned opinion has been sent" class="btn btn-opinion ipx-tag shadow-sm">
                <span class="tag-symbol">R</span>
                <span class="tag-count">1</span>
                <span class="sr-only">1 Reasoned opinion</span>
            </span>
        </a-tooltip>
        <a-tooltip placement="top" overlayClassName="ipx-tooltip" trigger="click">
            <template slot="title">
            <strong>1</strong> Political Dialogue
            </template>
            <span title="1 Political Dialogue" class="btn btn-dialog ipx-tag shadow-sm">
                <span class="tag-symbol">D</span>
                <span class="tag-count">1</span>
                <span class="sr-only">1 Political Dialogue</span>
            </span>
        </a-tooltip>
    </div>
</div>
```