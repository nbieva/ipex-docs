---
title: NP materials
lang: fr-FR
---

# National Parliaments materials

<a data-fancybox="parliament" title="" href="/assets/parl.jpg">![](/assets/parl.jpg)</a>

In order to fill all the necessary areas of the IPEX V3, each National Parliament should provide the following:

## Text materials

+ A **name**
+ A **summary** (between 200 and 300 characters).
+ A **long description** (Min 1200 characters).
+ A list of **Parliaments websites** URLs (Min 1 item) with, for each, the **name** and the **URL**.
+ **Social medias** accounts URLs

## Visual materials

+ The **logo of the Parliament**. Please provide a vector [SVG](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics) file or a Hi-Res PNG of min 1200px wide, with transparency if needed.
+ **Variations of the logo** if there are some (black/white, inverted, without text, etc.).
+ Minimum **two pictures of a plenary** with a short description for each.
+ Minimum **two pictures of the building** with a short description for each.
+ All these pictures will be part of the Parliament Gallery.
+ A preferred **banner image** (for the parliament details page). You can choose this image in the gallery images, as the banner image may be included in the gallery. If there is no banner image set for a Chamber, the default IPEX banner image will be displayed.
+ Each picture should be min 1200px wide, preferrably landscape format. Crop/resize your images if needed.

# Conference materials

<a data-fancybox="conference" title="" href="/assets/conf.jpg">![](/assets/conf.jpg)</a>

In order to fill all the necessary areas of the IPEX V3, each Conference manager should provide the following:

+ The **name** of the conference.
+ The **description** of +- 200 characters.
+ The **logo** of the conference if there is one. Please provide a vector [SVG](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics) file or a Hi-Res PNG of min 1200px wide, with transparency if needed.
+ **Variations of the logo** if there are some (black/white, inverted, without text, etc.).
+ At least **one family picture** to be displayed in the conference header. Please provide a Hi-Res JPG or PNG of min 1200px wide. Landscape format is preferred.
+ At least **4 pictures** for the conference gallery (About tab) with a short description for each image.
+ A preferred **banner image** (for the conference page). You can choose this image in the gallery images, as the banner image may be included in the gallery. If there is no banner image set for a Conference, the default IPEX banner image will be displayed.

Please provide for each image the license/author/copyrigth if there is one to mention.

If you have any question about file formats or image quality, please do not hesitate to contact us. We will do our best to help you providing these materials.

--------------

<a data-fancybox="sizes" title="" href="/assets/np-materials.jpg">![](/assets/np-materials.jpg)</a>