---
title: Additional image for meetings
lang: fr-FR
---

# Additional image for meetings

<p class="lead">This page concerns an additional image to be added to the meetings of a conference, to fit what was in V2. The image will be shown (when there is one) with the description of the meeting.</p>


+ Show only if specified.
+ As other images, it should be expandable to get a larger version of the image. Use for this a [plugin like Fancybox](https://fancyapps.com/fancybox/3/). We really need this.

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/conferences/conference/">Demo</a>

## With the visual of the meeting:

<a data-fancybox="meeting" title="" href="/assets/meeting-01.jpg">![](/assets/meeting-01.jpg)</a>

The following code uses [Fancybox](https://fancyapps.com/fancybox/3/).

### HTML markup

```html
<section class="d-flex flex-column flex-md-row align-items-start text-center text-md-left">
    <figure class="has-zoom meeting-illu ml-md-5 order-md-1 mb-4 mb-md-0">
        <a data-fancybox="gallery" href="/img/samples/sample.jpg">
            <img src="/img/samples/sample.jpg" alt="Sample image">
            <i class="material-icons md-36">
            zoom_in
            </i>
        </a>
        <figcaption>This is a sample description of the image for this meeting.</figcaption>
    </figure>
    <div>
        <h2 class="section-title">Austria 2019</h2>
        <p class="lead">Webtwo ipsum orkut reddit meebo skype vimeo jajah spock empressr zimbra, mobly napster hipmunk prezi chartly bitly spock. Loopt twones meebo hipmunk, fleck xobni. Convore bebo rovio vimeo zanga handango blekko koofers, loopt twitter imvu flickr kaboodle chegg.</p>
    </div>
</section>
```

## Without the visual of the meeting:

<a data-fancybox="meeting" title="" href="/assets/meeting-02.jpg">![](/assets/meeting-02.jpg)</a>

### HTML markup

```html
<section class="d-flex flex-column flex-md-row align-items-start text-center text-md-left">
    <div>
        <h2 class="section-title">Austria 2019</h2>
        <p class="lead">Webtwo ipsum orkut reddit meebo skype vimeo jajah spock empressr zimbra, mobly napster hipmunk prezi chartly bitly spock. Loopt twones meebo hipmunk, fleck xobni. Convore bebo rovio vimeo zanga handango blekko koofers, loopt twitter imvu flickr kaboodle chegg.</p>
    </div>
</section>
```

## Less

Less styles are located in **parliaments-conferences.less**