---
title: Login page
lang: fr-FR
---

# Login page

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/myipex/login">Demo</a>

<a data-fancybox="share" title="" href="/assets/login.png">![](/assets/login.png)</a>

## HTML markup

```html
<div class="container-regular flex-grow-1 ipx-content">
    <section>
        <form>
            <div class="form-row">
                <h2 class="section-title col-12">Your credentials</h2>
                <div class="form-group col-md-6">
                    <label for="inputEmail">Email</label>
                    <input type="email" class="form-control" id="inputEmail" placeholder="Your email" value="email@example.com" required>
                    <small class="validation-text">Please enter your email</small>
                </div>
                <div class="form-group col-md-6 has-error">
                    <label for="inputPassword">Password</label>
                    <input type="password" class="form-control" id="inputPassword" placeholder="Password" value="lemotdepasse" required>
                    <small class="validation-text">Your password does not match</small>
                </div>
                <div class="form-group col-12 pt-3 d-flex justify-content-between align-items-start">
                    <div class="forgot">
                        <a class="d-flex align-items-center mb-2" href="">Forgot your password?</a>
                        <a class="d-flex align-items-center mb-2" href="">Create an account</a>
                    </div>
                    <button  class="btn btn-action btn-flash btn-large d-flex align-items-center">
                        Log in
                    </button>
                </div>
            </div>
        </form>
    </section>
</div>
```