---
title: ECPRD
lang: fr-FR
---
Pour Tanisha

## HTML markup for List of requests

```html
<ul class="list-of-requests lgi-group-flex">
    ...
</ul>
```

## HTML markup for an request item

```html
<li class="lgi-flex">
    <a href="#" title="View this request" class="lgi-primary">
        <span class="request-number">3501</span>
        <div>
            <h3 class="request-reference">Rules regulating implementation of guidelines on corporate identity/visual standards/logo of the parliament</h3>
            <h4 class="name-of-parliament">MOLDOVA - ASSEMBLEE DE LA REPUBLIQUE DE MOLDAVIE</h4>
        </div>
    </a>
    <div class="lgi-secondary">
        <div class="lgi-metas">
            <span>
                Published on : 06/12/2018
            </span>
            <span>
                Deadline : 27/09/2018
            </span>
        </div>
    </div>
</li>
```

## HTML markup for an request item with GET final summary button

```html
<li class="lgi-flex">
    <a href="#" title="View this request" class="lgi-primary">
        <span class="request-number">3501</span>
        <div>
            <h3 class="request-reference">Rules regulating implementation of guidelines on corporate identity/visual standards/logo of the parliament</h3>
            <h4 class="name-of-parliament">MOLDOVA - ASSEMBLEE DE LA REPUBLIQUE DE MOLDAVIE</h4>
        </div>
    </a>
    <div class="lgi-secondary">
        <div class="lgi-btns">
            <button class="btn-flex bg-default btn-icon-left"><i class="material-icons">arrow_downward</i>Get final summary</button>
        </div>
        <div class="lgi-metas">
            <span>
                Published on : 06/12/2018
            </span>
            <span>
                Deadline : 27/09/2018
            </span>
        </div>
    </div>
</li>
```

## HTML markup for an request item with ADD final summary button

```html
<li class="lgi-flex">
    <a href="#" title="View this request" class="lgi-primary">
        <span class="request-number">3501</span>
        <div>
            <h3 class="request-reference">Rules regulating implementation of guidelines on corporate identity/visual standards/logo of the parliament</h3>
            <h4 class="name-of-parliament">MOLDOVA - ASSEMBLEE DE LA REPUBLIQUE DE MOLDAVIE</h4>
        </div>
    </a>
    <div class="lgi-secondary">
        <div class="lgi-btns">
            <button class="btn-flex bg-default btn-icon-left"><i class="material-icons">add</i>Add final summary</button>
        </div>
        <div class="lgi-metas">
            <span>
                Published on : 06/12/2018
            </span>
            <span>
                Deadline : 27/09/2018
            </span>
        </div>
    </div>
</li>
```

## SCSS rules for _22_tosass.scss

```scss
/* Hide specific widget on home / news - Maybe hardcode / maybe in db - see that later - Sebastien */

div[data-widget-id="3"] .column,
div[data-widget-id="19"] {
    display: none;
}

.left-panel ul li:nth-child(8) a:after {
    content:"New";
    background:#2c7cc0;
    font-size:11px;
    text-transform:uppercase;
    border-radius:3px;
    margin-left:8px;
    padding:1px 4px;
    vertical-align:top;
    margin-top:4px;
    display:inline-block;
  }
  
  .search-database-filters {
      -webkit-box-pack: end;
      -webkit-justify-content: flex-end;
      -ms-flex-pack: end;
      justify-content: flex-end;
      -webkit-box-orient: horizontal;
      -webkit-box-direction: reverse;
      -webkit-flex-direction: row-reverse;
      -ms-flex-direction: row-reverse;
      flex-direction: row-reverse;
  }
  #add-filter-div {
      width:170px;
  }
  .criteriaDivContainer {
      max-width:calc(100% - 170px);
      -webkit-box-flex: 1;
      -webkit-flex-grow: 1;
      -ms-flex-positive: 1;
      flex-grow: 1;
  }
  
  @media screen and (max-width:800px) {
      body h2.truncate {
          width:calc(100% - 160px);
      }
  }
  @media screen and (max-width:639px) {
      #add-filter-div,
      .criteriaDivContainer {
          width:100%;
          max-width:100%;
      }
      #add-filter-div {
          text-align:center;
      }
      .search-database-filters .btn-add-filter {
          background:#f9f9f9;
          width:60%;
          float:none;
          padding:15px;
      }
  }
  body.panel-left-open.right-panel-open .content {
      width:100%;
  }
  body.right-panel-open .main-wrapper {
      width: 100%;
  }
  body.panel-left-open.right-panel-open .main-wrapper {
      width: calc(100% - 300px);
  }
  .advanced-options {
      display:none;
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  @media screen and (max-width:639px) {
  
      .advanced-options {
          width: 100%;
      }
      
      .advanced-options-fields {
          padding:0 20px 25px;
      }
      
      .advanced-options-fields > div {	
          padding:10px 0 0 0;		
          -webkit-flex-wrap: wrap;		
          -ms-flex-wrap: wrap;
          flex-wrap: wrap;	
      }
      
      .advanced-options-fields label {	
          font-weight: 600;
          width: 100%;
      }
  }
  
  
  .filter-text .filter-content {
      padding:5px 15px 25px;
  }
  .filter-text label {
      color:#666;
      font-size:0.9em;
      font-weight:400;
      padding-top:15px;
  }
  
  .filter-text .filter-content input {
      width: 100%;
      padding: 7px 10px;
      font-size: .9em;
      font-weight: 400;
      border: 0;
      border-radius: 2px;
  }/*
  #add-filter-div {
      width:100%;
  }*/
  
  .right-panel .selected-filters li {
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      
      -webkit-box-align: center;
       -webkit-align-items: center;
       -ms-flex-align: center;
       align-items: center;
      
      padding-right:4px;
  }
  .right-panel .selected-filters li.criteria-must:after,
  .right-panel .selected-filters li.criteria-not:after {
      text-transform:uppercase;
      font-weight:600;
      background:#dceeff;
      color:#26649b;
      border-radius:3px;
      font-size:10px;
      padding:0 3px;
      line-height:13px;
      margin-left:5px;
  }
  .right-panel .selected-filters a, 
  .right-panel .selected-filters li {
      opacity:1;
  }
  .right-panel .selected-filters li.criteria-must:after {
      content:"Must";
  }
  .right-panel .selected-filters li.criteria-not:after {
      content:"Not";
  }
  .filter-text-buttons {
  text-align:right;
  }
  .filter-text-buttons button {
      border:none;
      padding:3px 12px;
      border-radius:3px;
      color:#f1f1f1;
      background:#000;
      margin-top:15px;
  }
  .filter-text {
      margin-top:20px;
  }
  .filter-empty .input-wrapper {
      display:none;
  }
  .noresult {
      color:#666;
      text-align:center;
      font-size:13px;
  }

  #page-search .content {
      padding:0;
  }
  .filter-content .list-wrapper ul {
      max-height:280px;
  }
  .your-criteria {
      height:200px;
  }
  .right-panel .selected-filters {

    max-height: 80px;
    overflow-y: scroll;
}
.tempnote p {

    background: url(https://ecprdpp.secure.europarl.europa.eu/ecprd/images/material-icons/lightbulb_outline_24px.svg);
        background-position-x: 0%;
        background-position-y: 0%;
        background-repeat: repeat;
    background-repeat: no-repeat;
    background-position: 0 50%;
}
@media screen and (max-width:767px) {
    .banner {
      min-height: 0;
      height: 0;
      display:none;
  }
  }

  @media screen and (min-width: 1100px) {
    .cards-box[data-columns]:before, 
    .dashboard-cards-box[data-columns]:before {
        content: '3 .column.size-1of3';
        display:none;
    }
  }

  /* ////////////////////////////////////////////////////////////// */
  /* ///////////////////////// PASSWORD /////////////////////////// */
  /* ////////////////////////////////////////////////////////////// */

// FORGOT PASSWORD

.account-menu-additional {
    padding:15px;
    div {
        padding-top:15px;
        border-top:1px solid #ccc;
    }
    p {
        color:$blue;
    }
    input {
        height:40px;
        font-size:14px !important;
        border-radius:3px;
        &:hover,
        &:focus {
            font-size:14px !important;
        }
    }
    button {
        &.btn {
            width:100%;
            padding:10px 15px;
            margin-top:15px;
    border-radius:3px;
        }
    }
}

// SELECT TABLE

.selecttable {
    float:left;
    display:flex;
    width:100%;
    flex-direction:column;
    border-bottom:1px solid $grayD;
    border-left:1px solid $grayD;
    background:white;
    border-radius:5px;
    .st-list {
        max-height:400px;
        overflow-y:scroll;
    }
    .st-row,
    .st-header {
        display:flex;
        justify-content: space-between;
        span {
            display:flex;
            align-items:center;
            justify-content: center;
            min-width:160px;
            padding:10px 15px;
            border:1px solid $grayD;
            border-left:none;
            border-bottom:none;
           /* &:first-child {
                width:40%;
                justify-content: flex-start;
                flex-grow:1;
            }*/
            &:first-child {
                &::before {
                    display: inline-block;
                    content: "";
                    color: #053b67;
                    display: -webkit-inline-box;
                    display: -ms-inline-flexbox;
                    display: inline-flex;
                    background: white;
                    width: 16px;
                    height: 16px;
                    margin-right: 8px;
                    -webkit-box-pack: center;
                    -ms-flex-pack: center;
                    justify-content: center;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center;
                    border: 1px solid #ccc;
                    border-radius: 2px;
                }
            }
        }
    }
    .st-header {
        background:#e5e5e5;
        font-weight:600;
        span {
            &:hover {
                cursor:pointer;
                color:$blue;
            }
            &::after {
                content: "^";
                transform: rotate(180deg);
                margin-left:5px;
                font-family: verdana;
                font-size:0.9em;
            }
            &.selected {
                color:$blue;
            }
            &:first-child {
                display:flex;
                justify-content: center;
                align-items:center;
                width:50px;
                flex-grow:0;
                min-width:0;
                padding:0;
                &:before {
                    margin-right:0;
                }
                &::after {
                    display:none;
                }
                &.selected {
                    &::before {
                        color: white;
                        background: #1184e6;
                        border-color: #1184e6;
                        content: "\2713";
                    }
                }
            }
        }
        .sth-username {
            flex-grow:1;
            justify-content: flex-start;
        }
    }
    .st-row {
        span {
            i {
                font-style:normal;
                font-size:0.9em;
                background:#eee;
                padding:0 6px;
                border-radius:2px;
                display:flex;
                align-items:center;
                justify-content: center;
            }
            &:first-child {
                &::before {
                    margin-right:34px;
                }
            }
        }
        &:hover {
            background:#f9f9f9;
            cursor:pointer;
        }
        &.selected {
            background:#f5f9ff;
            font-weight:600;
            span {
                &:first-child {
                    &::before {
                        color: white;
                        background: #1184e6;
                        border-color: #1184e6;
                        content: "\2713";
                    }
                }
                i {
                    background:$blue;
                    color:white;
                }
            }
        }
        &.requested {
            background: #fcf7ee;
            color:$grayC;
            .st-username {
                &::after {
                    content: "Update requested";
                    color:$orange;
                    font-size: 12px;
                    font-weight: 600;
                    line-height: 12px;
                    margin-left: 15px;   
                }
            }
            span {
                &:first-child {
                    &::before {
                        content:'\2713';
                        color: #999;
                        border: 1px solid $grayC;
                        font-weight: 600;
                        background: $grayD;
                    }
                }
            }
        }
        .st-username {
            flex-grow:1;
            justify-content: flex-start;
        }
    }
}

.user-access-control {
    input.form-control {
        height:50px;
        padding:10px 15px;
        font-size:18px !important;
        border-radius:3px;
        margin-bottom:15px;
        margin-top:10px;
    }
}

.disabled-like {
    display:block;
    border-radius:3px;
    div {
        padding:8px 12px;
        margin-bottom:8px;
        background:#f1f1f1;
    }
    p {
        padding:8px 12px;
        color:#666;
        background:#f1f1f1;
      }
}

.password-history {
    margin-top:10px;
    > div {
        display:flex;
        justify-content: space-between;
        color:#666;
    }
    .h-date {
        font-weight:600;
    }
    .h-length,
    .h-complexity,
    .h-frequency {
        margin-left:8px;
    }
}

.modal-password {
    .modal-dialog {
        .modal-content {
            border-radius:6px;
        }
    }
    .modal-header {
        padding:25px 70px;
        border: none !important;
        text-align:center;
    }
    .modal-body {
        padding:30px;
        padding-top:20px;
        .form-group {
            margin-top: 0;
        }
        .form-control {
            background:#f1f1f1;
            height:45px;
            border-radius:3px;
            border:1px solid #ddd;
            box-shadow:none;
            font-size:36px !important;
            border:none !important;
        }
        &.password-ok {
            .form-control {
                background:#f0fcee !important;
                color:$green;
            }
        }
        &.password-notok {
            .form-control {
                background:#fceeee !important;
                color:$red;
            }
        }
    }
    .modal-footer {
        padding-bottom:25px;
        display:flex;
        justify-content: space-between;
        border:none;
        padding:20px;
        padding-top:10px;
        width:100%;
        .btn {
            padding: 8px 20px;
            font-size: 1.2em;
            margin-right: 10px;
            border-radius:2px;
        }
        .notification {
            flex-grow:1;
            display:flex;
            align-items:center;
            padding:0 15px;
            &.notok {
                color:$red;
            }
            &.ok {
                color:$green;
            }
        }
    }
}

.password-required {
    font-weight:600;
    input {
        margin-right:10px;
    }
}

/* TOPBOXES */

.topbox-requests {
    .form-group {
        padding:0 !important;
        > div {
            display:flex;
            align-items:center;
            label {
                padding-left:0;
                flex-shrink:0;
                margin:0;
  margin-right:10px;
            }
            input {
                width:200px;
                margin-right:15px;
            }
        }
    }
}
.topbox-subjects {
    .form-group {
        display:flex;
        align-items:center;
        justify-content:space-between;
        padding:0;
        .col-sm-6 {
            display:flex;
            align-items:center;
            .control-label {
                padding-left:0;
                margin:0;
                margin-right:10px;
                flex-shrink:0;
            }
            &:first-child {
                input {
                    flex-grow:1;
                }
            }
            &:last-child {
                justify-content:flex-end;
            }
        }
    }
}

/* /////////////////////////////////////////////////////////// */
/* CORRESPONDENT DASHBOARD */
/* /////////////////////////////////////////////////////////// */

.my-requests {
    padding:30px;
    font-size:16px;
    .mr-3 {
        margin-right:.75em;
    }
    @include tablet {
        padding:15px;
    }
    .progress {
        @include flex;
        height:6px;
        margin-bottom:8px;
        @include flex-shrink(0);
        background:$gray;
        border-radius:10px;
        .bg-success {
            background:$green;
            border-radius:10px;
        }
        .bg-warning {
            background:#ffda48;
            border-radius:10px;
        }
        .bg-danger {
            background:#f35959;
            border-radius:10px;
        }
    }
}

.lgi-group-flex {
    padding:0;
    .lgi-flex {
        @include flex;
        padding:1em;
        margin-bottom:1em;
        background:white;
        border-radius:.4em;
        transition:all ease .2s;
        @include tablet {
            padding:1em 1.5em 1.5em;
            @include flex-direction(column);
        }
        &:hover {
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
        }
    }
    .lgi-primary {
        @include flex;
        @include flex-grow(1);
        padding-right:2em;
        @include tablet {
            @include flex-direction(column);
            padding-right:0;
        }
    }
    .lgi-secondary {
        @include flex;
        @include flex-shrink(0);
        @include small-desktop {
            @include flex-direction(column);
        }
        @include tablet {
            margin-top:1em;
            @include flex-direction(row);
            @include flex-justify(space-between);
        }
        @include phone {
            @include flex-direction(column);
        }
    }
    .request-number {
        padding:0 .75em 0 .25em;
        color:black;
        font-size:1.4em;
        @include tablet {
            padding:0;
        }
    }
    .name-of-parliament {
        margin:.5em 0;
        color:$grayDark;
        font-size:.85em;
        .flag {
            margin-right:.6em;
        }
    }
    .lgi-progress {
        @include flex-shrink(0);
        @include flex-grow(1);
        width:180px;
        max-width:180px;
        margin:0 3em;
        @include small-desktop {
            width:100%;
            max-width:100%;
            margin:0;
            margin-bottom:.75em;
        }
        @include tablet {
            @include flex-shrink(1);
            width:180px;
            max-width:180px;
            margin-right:2em; 
        }
        @include phone {
            width:100%;
            max-width:100%;
            margin-top:.5em;
        }
        h6 {
            margin-bottom:.2em;
            font-size:.85em;
        }
        .progress {
            height:6px;
            margin-bottom:.75em;
            &:last-child {
                @include tablet {
                    margin-bottom:0;
                }
            }
        }
    }
    + .actions-buttons {
        @include flex;
        @include flex-justify(center);
        margin-bottom:2em;
    }
    h3, h4, h6 {
        padding:0;
        margin:0;
        font-size:1em;
        font-weight:400;
    }
    .lgi-btns {
        display:flex;
        flex-direction:column;
        @include flex-justify(end);
        @include flex-align(stretch);
        > div {
            @include flex;
            @include flex-justify(end);
            @include flex-align(start);
            @include flex-shrink(0);
            @include tablet {
                @include flex-align(end);
                margin-top:.5em;
            }
        }
        .btn-flex {
            font-size:1em;
            &.decision-noreply {
                margin-top:1em;
                background:white;
                font-size:.85em;
                color:#999;
                &:hover {
                    color:#111;
                }
            }
            @include phone {
                width:100%;
                padding:.5em;
                margin-top:1em;
                @include flex-justify(center);
            }
        }
        
    }
}
.actions-buttons {
    @include flex;
    @include flex-wrap(wrap);
    margin-bottom:2em;
    @include phablet {
        @include flex-justify(space-between);
    }
}
button {
    &.btn-soft {
        @include flex-shrink(0);
        padding:0.5em;
        padding-right:1em;
        margin-right:1em;
        font-size:1.1em;
        border:2px solid white;
        background:white;
        border-radius:.3em;
        transition:all ease .3s;
        &:hover {
            border-color:$blue;
        }
        @include phablet {
            @include flex-justify(center);
            width:calc(50% - .5em);
            margin:0;
            margin-bottom:.5em;
        }
        @include phone {
            width:100%;
        }
    }
}
.btn-flex {
    @include flex;
    @include flex-align(center);
    padding:0.25em .9em .25em .5em;
    color:black;
    border:none;
    font-size:.95em;
    background:white;
    border-radius:3px;
    box-shadow:none;
    transition:all ease .3s;
    &:hover {
        cursor:pointer;
    }
    &.bg-default {
        background:$gray;
        &:hover {
            background:$grayLight;
        }
    }
    &.bg-dark-default {
        background:$grayDark;
        color:white;
        &:hover {
            background:darken($grayDark,10%);
        }
    }
    &.bg-lightblue {
        background:#D6E3ED;
        &:hover {
            background:#c4d7e6;
        }
    }
    &.bg-blue {
        background:$blue;
        color:white;
        &:hover {
            background:$blue;
        }
    }
    .material-icons {
        padding:0;
        padding-right:8px;
        color:black;
    }
}

/* SPECIFIC STYLES WHEN LEFT PANEL IS OPEN ON MEDIUM SCREENS */

.panel-left-open {
    .lgi-group-flex {
        .lgi-flex {
            @include desktop {
                padding:1em 1.5em 1.5em;
                @include flex-direction(column);
                .lgi-primary {
                    @include flex-direction(column);
                    padding-right:0;
                }
                .lgi-secondary {
                    @include flex-direction(row);
                    @include flex-justify(space-between);
                    margin-top:1em;
                }
                .request-number {
                    padding:0;
                }
                .lgi-progress {
                    @include flex-shrink(1);
                    width:180px;
                    max-width:180px;
                    margin-right:2em;
                    margin-left:0;
                    .progress {
                        &:last-child {
                            margin-bottom:0;
                        }
                    }
                }
                .lgi-btns {
                    @include flex-align(end);
                    margin-top:.5em;
                }
            }
        }  
    }
}

/* /////////////////////////////////////////////////////////// */
/* FINAL SUMMARY VISIBILITY */
/* /////////////////////////////////////////////////////////// */

.request-intro {
    padding:15px 20px;
    font-size: 16px;
    border:none;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
    h3 {
        clear:none;
    }
    .request-title {
        clear:none;
        margin-bottom:10px;
        font-size:18px; 
        line-height:24px;
        strong {
            margin-right:12px;
            font-weight:700;
        }
        @include desktop {
            margin-right:40px;
        }
    }
    .chamber-name {
        font-size:16px;
    }
    .subject-areas-list {
        padding:0;
        margin:0;
        list-style-type: none;
        font-size:14px;
    }
    .info-title,
    .info-content {
        margin-bottom:5px;
        font-size:14px;
    }
    .info-title {
        padding-bottom:0;
        margin-top:10px;
        font-size:14px;
        font-weight:600;
    }
}
.request-intro-footer {
    @include flex;
    @include flex-wrap(wrap);
    @include flex-justify(space-between);
    @include flex-align(end);
    padding-bottom:4px;
    h6 {
        margin-bottom:3px;
        font-size:13px;
    }
    .progress {
        @include flex;
        @include flex-shrink(0);
        height:6px;
        margin-bottom:8px;
        background:#e5e5e5;
        border-radius:10px;
        .bg-success {
            background:$green;
            border-radius:10px;
        }
    }
    .request-actions {
        @include flex;
        @include flex-justify(end);
        @include flex-grow(1);
        padding:0;
        margin-top:15px;
        margin-bottom:8px;
        @include phablet {
            @include flex-direction(column);
            width:100%;
            padding:15px 0 0;
        }
        .btn-flex {
            @include flex;
            @include flex-justify(center);
            @include flex-shrink(0);
            padding:8px 15px 8px 15px;
            margin-left:12px;
            font-size:16px;
            border-radius:4px;
            @include phablet {
                @include flex-grow(1);
                margin-top:12px;
                margin-left:0;
            }
            @include phone {
                padding:12px 16px 12px 16px;
            }
            .material-icons {
                color:white;
            }
        }
    }
}
.pie-container {
    margin-top:20px;
}
.btn-summary {
    font-size:14px;
    background:#f1f1f1;
    margin-bottom:1em;
    .material-icons {
        padding:0;
        margin-right:5px;
    }
}
.list-of-requests {
    .material-icons {
        padding:0;
        padding-right:10px;
    }
    &.lgi-group-flex {
        .request-reference {
            color:black;
            font-weight:600;
            font-size:15px;
        }
        .name-of-parliament {
            color:black;
            font-size:14px;
        }
        .lgi-flex {
            margin-bottom:.75em;
            &:hover {
                .request-reference {
                    color:$blue !important;
                }
            }
        }
        .lgi-secondary {
            @include flex;
            @include flex-direction(column);
            @include flex-justify(between);
            padding:5px;
            @include tablet {
                @include flex-direction(row);
                margin-top:12px;
                margin-left:0;
                .lgi-buttons {
                    
                }
            }
            @include phone {
                @include flex-direction(column);
            }
            .lgi-btns {
                padding-bottom: 15px;
            }
            .lgi-metas {
                font-size: 0.9em;
                color: #999;
                margin-bottom: 0;
                @include flex;
                @include flex-direction(column);
                @include flex-align(end);
                @include tablet {
                    order:-1;
                    @include flex-align(start);
                }
            }
        }
    }
}

.topbox-requests {
    border-radius:4px;
}
```
