---
title: Empty cards in home carousels
lang: fr-FR
---

# Empty cards in home carousels

<p class="lead">Home carousel cards should always be multiple of 3. <strong>This was agreed in the work group meeting 27.05.2020.</strong>
Originally, cards were designed to fit the available space.
Now, we are going to display always 3 cards per row. If there is less than 3 empty spots will be filled with default gray boxes.</p>

<a data-fancybox="editor" title="" href="/assets/empty-cards-01.png">![](/assets/empty-cards-01.png)</a>

**Original ticket:** IPEXL-1352 [HomePage] Display in each carousel a multiple of 3 cards.

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/home/">Demo</a>

Each card, if empty, should be built with the following code. The only thing that changes is the ipx-card-content element. We add to it an **.empty-card-content** class and some span elements. 

This has to be implemented for all IPEX cards in home carousels:

+ Homepage: **Important issues**
+ Homepage: **High activity documents**
+ Homepage: **Upcoming events**
+ Homepage: **News** (we never know..)

::: tip
Note of course that **this needs the LESS files to be updated** in order to work correctly.
:::

The **ipx-card-content** code is the same for all cards and is not depending on the section. All changes (widths, heights, margins..) are in LESS files. But the addition of the **.empty-card-content** class is mandatory, as well as the number of span elements (3):

```html
<div class="ipx-card-content w-100 empty-card-content">
    <span></span>
    <span></span>
    <span></span>
</div>
```

### Important issues

<a data-fancybox="editor" title="" href="/assets/empty-cards-01.png">![](/assets/empty-cards-01.png)</a>

### Documents with high activity

<a data-fancybox="editor" title="" href="/assets/empty-cards-02.png">![](/assets/empty-cards-02.png)</a>

### Upcoming events

<a data-fancybox="editor" title="" href="/assets/empty-cards-04.png">![](/assets/empty-cards-04.png)</a>

### News

<a data-fancybox="editor" title="" href="/assets/empty-cards-03.png">![](/assets/empty-cards-03.png)</a>
