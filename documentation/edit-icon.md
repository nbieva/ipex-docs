---
title: Edit icon for the editor
lang: fr-FR
---
Edit icon for the editorNational Parliaments materials

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/parliaments/parliament-details">Demo</a>

<a data-fancybox="parliament" title="" href="/assets/edit-icon-02.png">![](/assets/edit-icon-02.png)</a>

<a data-fancybox="parliament" title="" href="/assets/edit-icon-01.jpg">![](/assets/edit-icon-01.jpg)</a>

## HTML markup

```html
<section class="editable">
    <i class="editable-icon material-icons" title="Open the editor">edit</i>
</section>
```

## LESS files

```less
/* //////////////////////// */
/* Edit icon for editor
/* //////////////////////// */
.editable {
  position:relative;
  .editable-icon {
    position:absolute;
    top:0;
    right:0;
    display:inline-flex;
    justify-content:center;
    align-items:center;
    height:36px;
    width:36px;
    padding:.3rem .8rem;
    font-size:20px;
    color:rgb(196, 196, 196);
    border-radius:5rem;
    background:white;
    transition:all ease .3s;
    animation: colorblink 3s infinite;
    border:4px solid rgb(196, 196, 196);
    &:hover {
      opacity:1;
      cursor:pointer !important;
      border:4px solid rgb(238, 82, 43);
      background: rgb(238, 82, 43);
      color:white !important;
    }
  }
  &:hover {
    .editable-icon {
      opacity:1;
      cursor:pointer !important;
    }
  }
}

@keyframes colorblink { 
  0% {}
  50% { 
    color:rgb(238, 82, 43);
    border:4px solid rgb(238, 82, 43); 
  } 
}
```