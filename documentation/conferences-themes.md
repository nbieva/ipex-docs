---
title: Conferences themes
lang: fr-FR
---

# Conferences themes

<p class="lead">Conferences themes are pre-designed layout to be used in IPEX Conferences. These are applied to the page with a specific class (like <strong>.conference-theme0</strong>) set to the <strong>.visual-layout</strong> element. <strong>.conference-theme0</strong> is the default theme.</p>

<a data-fancybox="conferences-themes" title="" href="/assets/conference-themes.jpg">![](/assets/conference-themes.jpg)</a>

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/conferences/themes-base/">Demo</a>
```html
<div class="d-flex flex-column flex-grow-1 visual-layout conference-theme0">
...
</div>
```

You may use you web inspector to change classes.



There is actually **14 different layouts**. Some may be modified or added according to the user's needs. Each layout set custom colors (text and/or background) for:

+ Large banner background (gradient)
+ Images backgrounds (images are slightly colored)
+ Open/close intro button background (top right)
+ Border-left color of the next-event module (intro)
+ Internal navigation
+ Footer top border color
