---
title: New IPEX logotype
lang: fr-FR
---

# New IPEX logotype

<p class="lead">The new IPEX logo was designed to keep IPEX identity while creating a more modern and up-to-date logotype. You will find below the main files to be used for IPEX website.</p>

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/conferences/themes-base/">Demo</a>
<a class="btn-demo btn-demo-light" href="/assets/ipex-imgs-14042020.zip">Download new logo files</a>



## IPEX logo in website header

The IPEX main logo is displayed in its negative version (on a dark background). Replace the **ipex-logo-light.svg** file with this one: [ipex-logo-light.svg](/assets/ipex-logo-light.svg)

<a data-fancybox="logo" title="" href="/assets/logo-01.png">![](/assets/logo-01.png)</a>

```less
/* navigation.less - line 131 */
.ipex-logo {
  width: 210px;
  max-width: 210px;
  margin-top:.75rem;
  + h6 {
    /* should be removed from html */
    display:none;
  }
}
```

## X version

The IPEX logo in its icon-only version needs also to be replaced. Replace the **logo-x.svg** file with this one: [logo-x.svg](/assets/logo-x.svg)

<a data-fancybox="logo" title="" href="/assets/logo-02.png">![](/assets/logo-02.png)</a>

```less
/* LOGO X - line 191 */
.ipex-logo {
  width:90px;
  height:auto;
}
```

---------------

<a class="no-shadow" data-fancybox="logo" title="" href="/assets/ipex-recto.jpg">![](/assets/ipex-recto.jpg)</a>