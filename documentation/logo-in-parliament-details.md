---
title: NP Logo on Parliament details page
lang: fr-FR
---

# NP Logo on Parliament details page

<p class="lead">When there is one, we should display the Parliament's logotype on the Parliament details page. This was not added (my mistake. I forgot as there is none in V2). We will add the logo along with the Parliament websites section, that will then require some adaptations. LESS files are updated (as from 17/04/20). You will find the HTML markup below. Also, we will need an entry on the backoffice for a Parliament to add his logo, with this small mention: "A lanscape oriented version of the logo is preferred if there is one."(proposal to be discussed).</p>

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.app/parliaments/parliament-details/">Demo</a>

<a data-fancybox="parllogo" title="" href="/assets/parl-logo.png">![](/assets/parl-logo.png)</a>

Note that the URL of the image is here just an example from the test application and should be replaced of course by another one that match the Angular file architecture.

## HTML markup

```html
<div class="d-flex align-items-center">
    <div class="alert parliament-websites shadow-sm d-flex w-100">
        <div class="d-flex flex-column flex-grow-1">
            <h5>Parliament website(s):</h5>
            <div class="d-flex flex-wrap">
                <a href="#" class="d-flex align-items-center">Bundestag (DE)<i class="material-icons md-18 ml-1">launch</i></a>
                <a href="#" class="d-flex align-items-center">Bundestag (FR)<i class="material-icons md-18 ml-1">launch</i></a>
                <a href="#" class="d-flex align-items-center">Bundestag (EN)<i class="material-icons md-18 ml-1">launch</i></a>
            </div>
        </div>
        <div>
            <img src="/img/parliaments/germany/bundestag/logo-bundestag-square.png" alt="German Bundestag">
        </div>
    </div>
</div>
```
