---
title: How it works
lang: fr-FR
---

# How it works

<p class="lead">Part of this page comes from the excellent article by Sam Vloeberghs about sharing on social media platforms.</p>

+ [Read the article by Sam Vloeberghs](https://samvloeberghs.be/posts/better-sharing-on-social-media-platforms-with-angular-universal)
+ More info about [sharing on Facebook and Twitter](https://css-tricks.com/essential-meta-tags-social-media/)
+ This thread is a very interesting ressource also: [https://stackoverflow.com/questions/19778620/provide-an-image-for-whatsapp-link-sharing](https://stackoverflow.com/questions/19778620/provide-an-image-for-whatsapp-link-sharing)

## The buttons

+ Twitter button: [https://developer.twitter.com/en/docs/twitter-for-websites/tweet-button/overview](https://developer.twitter.com/en/docs/twitter-for-websites/tweet-button/overview)
+ [Facebook button](https://developers.facebook.com/docs/plugins/share-button)
+ [LinkedIn](https://docs.microsoft.com/en-us/linkedin/consumer/integrations/self-serve/plugins/share-plugin)

The above article tells us more about this in an Angular project.

In order to take full advantage of this sharing module, we should have:

+ At least **one reference image** for each page that has good chances to be shared, as conferences, news, events, parliaments, etc..
+ a **default IPEX visual** for all other pages.
+ a **specific description** for the page. This has to be defined for every type of content in IPEX.
+ a **default IPEX description** for all other pages. This may be the IPEX description in the IPEX widget (bottom of landing pages)

"Social media platforms use your statically generated HTML to show a preview when sharing one of your pages on their feed. Therefore they parse the title and meta tags that can be found in the **head** tag."

## The OpenGraph Protocol

This is a very useful [thread](https://stackoverflow.com/questions/19778620/provide-an-image-for-whatsapp-link-sharing)

```html
<title>IPEX.eu</title>
<meta name="description" content="IPEX is a platform for the mutual exchange of information between the national Parliaments and the European Parliament concerning issues related to the European Union, especially in light of the provisions of the Treaty of Lisbon." />
<meta property="og:title" content="IPEX.eu" />
<meta property="og:url" content="https://ipextest.netlify.com" />
<meta property="og:description" content="IPEX is the platform for EU Interparliamentary Exchange">
<meta property="og:image" content="/img/sharing-image.jpg">
<meta property="og:image:alt" content="Plenary Session in European Parliament">
<meta property="og:image:width" content="600">
<meta property="og:image:height" content="400">
<meta property="og:type" content="website" />
<meta property="og:locale" content="en_GB" />
```

#### og:image
Image(JPG or PNG) with a size less than 300KB and minimum dimensions of 300 x 200. Use [Tiny PNG](https://tinypng.com/) to compress images.
```html
<meta property="og:image" content="//cdn.example.com/uploads/images/webpage_300x200.png">
```

#### og:type
In order for your object to be represented within the graph, you need to specify its type. Here's a list of the global types available: [http://ogp.me/#types](http://ogp.me/#types). You can also specify your own types.
```html
<meta property="og:type" content="website" />
```

#### og:locale
The locale of the resource. You can also use og:locale:alternate if you have other language translations available.
**This is important for IPEX.**

If you don't specify og:locale, it defaults to en_US.
```html
<meta property="og:locale" content="en_GB" />
<meta property="og:locale:alternate" content="fr_FR" />
<meta property="og:locale:alternate" content="es_ES" />
```

## Facebook

Uses OpenGraph protocol.

+ This may be useful: [https://developers.facebook.com/docs/sharing/webmasters](https://developers.facebook.com/docs/sharing/webmasters)
+ Also this: [https://developers.facebook.com/docs/plugins/share-button/?locale=fr_FR](https://developers.facebook.com/docs/plugins/share-button/?locale=fr_FR)

```html
<head>
    <meta property="og:title" content="Insert here the title of the page">
    <meta property="og:description" content="Insert here the description of the page">
    <meta property="og:url" content="https://ipex.eu/">
    <meta property="og:image" content="Insert here the path to the illustration image">
    <meta property="og:image:alt" content="Insert here the description of the image (alt)">
    <meta property="og:image:height" content="xxx">
    <meta property="og:image:width" content="xxx">
</head>
```

And the javascript part:

```js
 <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  </script>
```

#### Validation for Facebook

Validating a preview of your post on Facebook can be done via [https://developers.facebook.com/tools/debug/sharing](https://developers.facebook.com/tools/debug/sharing). Facebook also provides a way to invalidate a batch of pages, which can also be done programmaticaly via their API.

## Sharing on LinkedIn

The following are from LinkedIn specifications:

You'll need to make sure the source code complies with [Open Graph Protocol (OGP)](https://ogp.me/) and certain image requirements specific to LinkedIn. Our developer website contains more details on [setting display tags for shares](https://docs.microsoft.com/en-us/linkedin/consumer/integrations/self-serve/share-on-linkedin).

#### Below are the og: tags that must exist and their correct format:

```html
<head>
    <meta property="og:title" content="Title of the article"/>
    <meta property="og:image" content="//media.example.com/ 1234567.jpg"/>
    <meta property="og:description" content="Description that will show in the preview"/>
    <meta property="og:url" content="//www.example.com/URL of the article"/>
</head>
```

#### Here are the image requirements specific to the LinkedIn sharing module:

+ Max file size: 5 MB
+ Minimum image dimensions: 1200 (w) x 627 (h) pixels
+ Recommended ratio: 1.91:1

Note: If the image meets the requirements, but it still does not appear in updates on LinkedIn, your website may be blocking us from pulling the image or the image may be located on a protected directory or website.

## Sharing on Twitter

+ [https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started)

"Twitter card tags look similar to Open Graph tags, and are based on the same conventions as the Open Graph protocol. When using Open Graph protocol to describe data on a page, it is easy to generate a Twitter card without duplicating tags and data. When the Twitter card processor looks for tags on a page, it first checks for the Twitter-specific property, and if not present, falls back to the supported Open Graph property. This allows for both to be defined on the page independently, and minimizes the amount of duplicate markup required to describe content and experience." ([Source](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started))

## Sharing on Whatsapp

### With a preview

To get a preview of IPEX on Whatsapp, we have to **use the OGP** and its constraints (see above).

### Prefilled text message

To open a WhatsApp chat with a pre-filled message, you can use a custom URL scheme. Opening **whatsapp://send?text=** followed by the text to send, will open WhatsApp, allow the user to choose a contact, and pre-fill the input field with the specified text.

You can Encode/Decode your text [here](https://www.url-encode-decode.com/)

#### Example:
```html
<a href="https://wa.me/?text=urlencodedtext">Share</a>
```
#### In IPEX:
```html
<a href="https://wa.me/?text=IPEX.eu+%7C+Belgian+Senate%0D%0ABelgium+is+a+federal+state+with+a+federal+parliament+consisting+of+two+assemblies%2C+the+House+of+Representatives+%28Chambre+des+repr%C3%A9sentants+%E2%80%93+Kamer+van+volksvertegenwoordigers%29+and+the+Senate+%28S%C3%A9nat+%E2%80%93+Senaat%29." class="share-whatsapp" title="Share on Whatsapp">
    <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512' viewBox='0 0 512 512'><title>Share on Whatsapp</title><path d='M414.73,97.1A222.14,222.14,0,0,0,256.94,32C134,32,33.92,131.58,33.87,254A220.61,220.61,0,0,0,63.65,365L32,480l118.25-30.87a223.63,223.63,0,0,0,106.6,27h.09c122.93,0,223-99.59,223.06-222A220.18,220.18,0,0,0,414.73,97.1ZM256.94,438.66h-.08a185.75,185.75,0,0,1-94.36-25.72l-6.77-4L85.56,427.26l18.73-68.09-4.41-7A183.46,183.46,0,0,1,71.53,254c0-101.73,83.21-184.5,185.48-184.5A185,185,0,0,1,442.34,254.14C442.3,355.88,359.13,438.66,256.94,438.66ZM358.63,300.47c-5.57-2.78-33-16.2-38.08-18.05s-8.83-2.78-12.54,2.78-14.4,18-17.65,21.75-6.5,4.16-12.07,1.38-23.54-8.63-44.83-27.53c-16.57-14.71-27.75-32.87-31-38.42s-.35-8.56,2.44-11.32c2.51-2.49,5.57-6.48,8.36-9.72s3.72-5.56,5.57-9.26.93-6.94-.46-9.71-12.54-30.08-17.18-41.19c-4.53-10.82-9.12-9.35-12.54-9.52-3.25-.16-7-.2-10.69-.2a20.53,20.53,0,0,0-14.86,6.94c-5.11,5.56-19.51,19-19.51,46.28s20,53.68,22.76,57.38,39.3,59.73,95.21,83.76a323.11,323.11,0,0,0,31.78,11.68c13.35,4.22,25.5,3.63,35.1,2.2,10.71-1.59,33-13.42,37.63-26.38s4.64-24.06,3.25-26.37S364.21,303.24,358.63,300.47Z' style='fill-rule:evenodd'/></svg>
</a>
```

## Sharing on Instagram

Does it really make sense for IPEX?

## Sharing by Email

You may use the mailto protocol in order to share your page by email directly. Email, subject and content are customizable through the href attribute.

```html
<a href="mailto:example@example.com?subject=IPEX.eu | Belgian Senate&body=Belgium is a federal state with a federal parliament consisting of two assemblies, the House of Representatives (Chambre des représentants – Kamer van volksvertegenwoordigers) and the Senate (Sénat – Senaat)." class="share-email" title="Send by email">
    <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512' viewBox='0 0 512 512'><title>Send by email</title><path d='M424,80H88a56.06,56.06,0,0,0-56,56V376a56.06,56.06,0,0,0,56,56H424a56.06,56.06,0,0,0,56-56V136A56.06,56.06,0,0,0,424,80Zm-14.18,92.63-144,112a16,16,0,0,1-19.64,0l-144-112a16,16,0,1,1,19.64-25.26L256,251.73,390.18,147.37a16,16,0,0,1,19.64,25.26Z'/></svg>
</a>
```

## Copy page URL to clipboard

To copy an URL to the clipboard requires a bit of javaScript. Please make sure there is no such thing already in Angular before implementing. I would not be surprised.

+ This is the idea: [https://www.w3schools.com/howto/howto_js_copy_clipboard.asp](https://www.w3schools.com/howto/howto_js_copy_clipboard.asp)
+ React third party: [https://github.com/nkbt/react-copy-to-clipboard](https://github.com/nkbt/react-copy-to-clipboard)

```html
<a href="#" class="share-copy" title="Copy link" onclick="copyLink()">
    <svg xmlns='http://www.w3.org/2000/svg' width='512' height='512' viewBox='0 0 512 512'><title>Copy link</title><path d='M408,480H184a72,72,0,0,1-72-72V184a72,72,0,0,1,72-72H408a72,72,0,0,1,72,72V408A72,72,0,0,1,408,480Z'/><path d='M160,80H395.88A72.12,72.12,0,0,0,328,32H104a72,72,0,0,0-72,72V328a72.12,72.12,0,0,0,48,67.88V160A80,80,0,0,1,160,80Z'/></svg>
</a>
```

```js
function copyLink() {
  /* fill variable */
  var copyText = "http://ipex.eu/lasection/lapage";

  copyText.setSelectionRange(0, 99999); /*For mobile devices*/
  document.execCommand("copy");

// Notify user that text is copied.
  alert("URL in clipboard: " + copyText.value);
}
```
