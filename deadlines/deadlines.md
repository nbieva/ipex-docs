---
title: Subsidiarity page
lang: fr-FR
---

# Subsidiarity page

<p class="lead">The Deadlines page displays the content that was displayed on the IPEX v2 homepage.</p>



<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/documents/deadlines">Demo</a>

<a data-fancybox="deadlines" title="" href="/assets/deadlines-intro.png">![](/assets/deadlines-intro2.png)</a>

```html
<div class="container-regular ipx-content">
    <section class="deadlines is-expiring">
        <!-- Section: Expiring indicative deadlines -->
    </section>
    <section class="deadlines">
        <!-- Section: New procedures - Subject to a deadline -->
    </section>
</div>

```

## Expiring indicative deadlines

<a data-fancybox="deadlines" title="" href="/assets/deadline-detail-1.png">![](/assets/deadline-detail-1.png)</a>

```html
<!-- The class .deadlines is used for both sections. The class .is-expiring is used only for this first section. -->
<section class="deadlines is-expiring">
    <!-- The section title is used to show/hide the section content -->
    <h2 role="button" class="section-title ipx-collapse-section-title d-flex align-items-start justify-content-between mt-0">
        Expiring indicative deadlines
        <button type="button" class="btn d-inline-flex justify-content-center align-items-center p-0 ml-4">
            <i class="material-icons md-36">keyboard_arrow_down</i>
        </button>
    </h2>
    <div class="ipx-card-list">
        <!-- This .ipex-card should be used for each item -->
        <div class="ipx-card ipx-card-exp" class="is-expiring">
            <a href="/" class="ipx-card-content">
                <h3 class="ipx-card-title d-flex flex-column flex-sm-row align-items-start align-items-sm-center">
                    <span class="flex-shrink-0 flex-grow-1">{{item.reference}}</span>
                    <span class="date d-flex align-items-center mt-1 mt-sm-0"><i class="material-icons">arrow_forward</i>{{item.deadline}}</span>
                </h3>
                <p class="ipx-card-description">
                    {{item.title}}
                </p>
                <div class="d-flex justify-content-between p-0 flex-wrap">
                    <div class="deadline-activity">
                        <h4>Documents</h4>
                        <ul>
                            <li v-for="(element, index) in item.documents" :key="index">
                                <a href="#" :title="'View Document '+element">{{element}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <!-- The View all button -->
    <div class="text-center">
        <button class="btn btn-load">
            View all expiring deadlines
        </button>
    </div>
</section>
```

## New procedures - Subject to a deadline


<a data-fancybox="deadlines" title="" href="/assets/deadline-detail-2.png">![](/assets/deadline-detail-2.png)</a>

```html
<section class="deadlines">
    <h2 role="button" class="section-title ipx-collapse-section-title d-flex align-items-start justify-content-between mt-0">
        New procedures - Subject to a deadline
        <button type="button" class="btn d-inline-flex justify-content-center align-items-center p-0 ml-4">
            <i class="material-icons md-36">keyboard_arrow_down</i>
        </button>
    </h2>
    <div class="ipx-card-list">
        <div class="ipx-card ipx-card-exp">
            <a href="/" class="ipx-card-content">
                <h3 class="ipx-card-title d-flex flex-column flex-sm-row align-items-start align-items-sm-center">
                    <span class="flex-shrink-0 flex-grow-1">{{item.reference}}</span>
                    <span class="date d-flex align-items-center mt-1 mt-sm-0"><i class="material-icons">arrow_forward</i>{{item.deadline}}</span>
                </h3>
                <p class="ipx-card-description">
                    {{item.title}}
                </p>
                <div class="d-flex justify-content-between p-0 flex-wrap">
                    <div class="deadline-activity">
                        <h4>Documents</h4>
                        <ul>
                            <li v-for="(element, index) in item.documents" :key="index">
                                <a href="#">{{element}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <!-- The View all button -->
    <div class="text-center">
        <button class="btn btn-load">
            New procedures with deadlines
        </button>
    </div>
</section>
```

## The View more button

The View more button shares styles with the usual Load more button that we may find below search results, etc.

```html
<div class="text-center">
    <button class="btn btn-load">
        View all expiring deadlines
    </button>
</div>
```

## LESS

```less
.deadlines {
  .ipx-card-list {
    padding:0;
  }
  h4 {
    font-size:1.1rem;
    font-weight:700;
    margin-bottom:.5rem;
    padding-top:1rem;
  }
}

.ipx-card-exp {
  position:relative;
  .ipx-card-title {
      display:flex;
      align-items:center;
      color:black;
  }
  .date {
    font-size:1rem;
    color:darken(@green, 7%);
    font-weight:700;
    margin-top:-10px;
    .material-icons {
      margin-right:.25rem;
    }
  }
  &::before {
      content:"";
      position:absolute;
      top:-2px;
      left:-2px;
      bottom:-2px;
      width:6px;
      border-radius:.3rem 0 0 .3rem;
      background:lighten(@green, 58%);
      background:  linear-gradient(194deg, lighten(@green, 20%) 0%, #9edcb3 100%);
  }
  &:hover {
    border-color:lighten(@green, 15%);
    &::before {
      border-radius:.3rem 0 0 .3rem;
      background:lighten(@green, 58%);
      background:  linear-gradient(194deg, lighten(@green, 10%) 0%, #9edcb3 100%);
    }
    .ipx-card-title {
      color:black;
  }
  }
  .ipx-card-content {
      padding-bottom:.7rem;
    }
}

.is-expiring {
  .ipx-card-exp {
    .date {
      color:@red;
    }
    &::before {
        content:"";
        background:lighten(@red, 58%);
        background:  linear-gradient(194deg, lighten(@red, 5%) 0%, #ff7a33 100%);
    }
    &:hover {
      border-color:lighten(@red, 15%);
      &::before {
        background:lighten(@red, 58%);
        background:  linear-gradient(194deg, lighten(@red, 5%) 0%, #ff7a33 100%);
      }
    } 
  }
}
.deadline-activity {
  ul {
    display:flex;
    list-style-type: none;
    margin-left:-.25rem;
    margin-bottom:0;
    li {
      a {
        display:inline-flex;
        align-items:center;
        background:#f9f9f9;
        font-size:.9rem;
        padding:.1rem .5rem;
        margin-right:.5rem;
        margin-bottom:.5rem;
        border-radius:3rem;
        transition:all ease .2s;
        &:hover {
          background:#f1f1f1;
        }
      }
    }
  }
}
```