---
title: Files in IPEX
lang: fr-FR
---

# Files in IPEX

## Related Jira tickets:

+ [IPEXL-2901 : Display selected board keyword for file [MOCK-UP]](https://ep-jira-issue-tracking.in.ep.europa.eu/ep-jira/browse/IPEXL-2901)
+ [IPEXL-2875 : FO REST API for Important issues listing](https://ep-jira-issue-tracking.in.ep.europa.eu/ep-jira/browse/IPEXL-2875)

## What has been changed?

From now, all files in IPEX :

+ will use the **same data structure**.
+ will now use the **same components** (**files-list** and **file-item)** so this will be far easier to maintain and debug in the future.
+ will now use the new **format checker pipe**, to check the file format and assign the corresponding class to the element.
+ may have a **list of keywords** attached (that can be used as filters in some places). 
+ have a **Download button** so the action on click will be explicit to the user.

Obviously, this has still to be implemented but most of the code is ready for it.

<a data-fancybox="" title="" href="/assets/tooltip-1col.jpeg">![](/assets/tooltip-1col.jpeg)</a>

## Locations

Files can be found in those different locations in IPEX:

+ **Carousel important issues** (not currently displayed)
+ **Calendar detail** page
+ **Conference Home**
+ **Conference About**
+ **Document** page (other files)
+ **Scrutiny** page (files)
+ **OWN** page
+ **Parliament** detail - **Public files**
+ **Parliament** detail - **OWN files**
+ Search - **Conference result output**
+ Search - **Parliaments result output**

## Specific layouts

### The Home Important Issues Carousel

Note that the Home Important Issues Carousel will have a specific layout..

<a data-fancybox="" title="" href="/assets/important-carousel.png">![](/assets/important-carousel.png)</a>

### The search results

As well, Search results cards (in the Search section) use a specific markup. So are the files in that section.

For the Parliament files and Conference files cards, we also display the path (where the file comes from).

<a data-fancybox="" title="" href="/assets/search-files.png">![](/assets/search-files.png)</a>

## Capabilities

Files in IPEX share some common capabilities:

+ They **can be downloaded**. A *Download* button has been added for the action to be explicit to the user.
+ They **can be attached to a specific entity** (Document, Event..) Note that currently files cannot be attached to a News. But it would make sense to implement it so we would have a shared structure between News page and Event page for example...
+ User may attach **"Keywords"** to files (see below) In some places, keywords can be act as filters and trigger new request to Elastic search on click.

## Properties

Each file in IPEX should also have the following properties available. 

As we will use one component for this, each file in IPEX should use this structure with the properties named the same way (avoid having "format" somewhere, and "fileFormat" somewhere else.. Or "fileTypeTitle" and "title", etc..).

+ **id** : String
+ **lang** : String 
+ **title** : String 
+ **name** : String
+ **link** : String
+ **ranking** : Number
+ **size** : String 
+ **format** : String
+ **date** : String
+ **type** : String
+ **path** : Array
+ **fileTypeTitle**: String
+ **keywords** : Array

### In detail

+ **id** is a unique identifier like "8af85a28768fca99017690591c4f001d"
+ **lang** is the language of the file (EN, FR, ..)
+ **title** is the name to be displayed in the front. This one can be the same as the following "name" property. Or not.
+ **name** is the name of the file with its extension ("meeting_123456.docx"). Used to create the URL to download.
+ **link** is used to create the URL to download.
+ **ranking** is used to order the files if needed
+ **size** is the size of the file (in bytes?)
+ **format** is the format of the file (PDF, DOC, etc..). This should correspond to its extension, even if it is not automatic and this string is sent like this by the backend.
+ **date** is the creation date of the file. This corresponds to the date it was uploaded to the BO. (to be confirmed)
+ **type** is the origin of the file (its original location in the application). For example "CONF_EVENT" or "SCRUTINY"
+ **path** is used in Parliament files and conference files to show where the files comes from. It is a kind of "breadcrumb" used in the Search section results.
+ **fileTypeTitle** is a kind of category (File type) used in conference files, for example "Agenda of the event".
+ **keywords** is the list of keywords attached to the file.

## Questions :

+ Should we add files to the News entity (News detail page) ?
+ Should files be a separated entity in the IPEX Search? To have a specific section to search files and then filter by Parliament files, Event files, etc.. instead of having files being "included" in other sections?
+ Should we have a "Keyword" filter in the IPEX search each time we have files in the results list? (if those files have keywords, of course..)? That filter does not exist yet.

