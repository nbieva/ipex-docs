---
title: Components
lang: fr-FR
---

## Components

Files will be using 2 different components and one pipe (for the file format) :

1. A **list of files** component (using the file component also)
2. A **file** component, which displays a file and its attached metadata (see below)
3. A **format checker pipe**, to check the file format and assign the corresponding class to the element.

## Lists of files

The **files-list** component displays a list of files. It has by default the ***ipx-files-list*** class, and has some options if needed :

+ **hasInput** : Boolean
+ **hascolumns** : Boolean
+ **isBoxed** : Boolean

### hasInput

The **hasInput** option add a large input on top of the list in order to dynamically filter the files already displayed in the front. It may then be used for large lists of files but does not send any additional request to Elastic search for example.

This adds the class ***has-input*** to the element.

<a data-fancybox="" title="" href="/assets/filtering-input.png">![](/assets/filtering-input.png)</a>

### hasColumns

The **hasColumns** option displays your list in 2 columns. 

+ An orphan on the last row will occupy the entire width. 
+ *hasColumns* is responsive and will be 100% with (one column) on smaller devices.

This adds the class ***has-cols*** to the element.

<a data-fancybox="" title="" href="/assets/list-2-columns.png">![](/assets/list-2-columns.png)</a>

### isBoxed

The **isBoxed** option displays your files in shadowed cards. Avoid using this for large lists of files.

This adds the class ***is-boxed*** to the element.

<a data-fancybox="" title="" href="/assets/boxed-list.png">![](/assets/boxed-list.png)</a>

### Code

```html
<div *ngIf="hasInput" class="ipx-input-container ipx-input-container-large d-flex mb-4">
  <input
    type="text"
    placeholder="Filter list"
    class="form-control ipx-input-large"
  />
</div>
<div 
   class="ipx-files-list"
   [ngClass]="{
      'is-boxed': isBoxed, 
      'has-cols': hasColumns,
      'has-input': hasInput
   }"
>
   <!-- Insert here the file-item component -->
</div>
```

## Files

The **file-item** component displays a file. It has by default the ***ipx-file*** class and has some options if needed :

+ **filerKeywords**: Boolean

### filerKeywords

If set to *true*, the **filerKeywords** option gives your keywords the ability to filter the list by triggering a new search. The different places where this has to be activated are still to be defined.

This adds the class ***is-filtering*** to the element.

```html
<div class="file-keywords">
   <span class="file-keyword">Brexit</span>
   <span class="file-keyword inuse">COVID-19</span>
</div>
```

If set to *true*, each time a keyword will be selected and active, the class ***inuse*** should be added to the keyword element (see below). Clicking again on the keyword will deselect it and deactivate the filter (triggers a new search).

### Code

```html
<div class="ipx-file">
   <div>
      <a [href]="path + 'download/file/' + id" [title]="name" download>
         {{ title | parseHtml }}
      </a>
      <div class="file-metas d-flex align-items-center">
         <span
         class="file-type"
         [ngClass]="format|fileFormatChecker">
            {{ format }}
         </span>
         <div class="lang-tags">
            <span>{{ lang | lowercase }}</span>
         </div>
         <span class="file-size">
            {{ size | fileSize: "MB" }}
         </span>
         <span class="file-date">
            {{ date | dateTimeFormatFilter: 'DD/MM/YYYY' }}
         </span>
      </div>
      <div class="file-keywords" *ngIf="keywords.length">
         <!-- Use 'inuse' class when keyword is active as a filter-->
         <span class="file-keyword" *ngFor="let keyword of keywords">
            {{ keyword }}
         </span>
      </div>
  </div>
  <div class="file-actions">
      <a 
         href="#" 
         class="download-button" 
         nz-tooltip
         [nzTitle]="downloadTemplate"
         nzPlacement="top"
         nzOverlayClassName="ipx-tooltip ipx-tooltip-align-left"
         nzTrigger="hover"
         download>
         <span class="material-icons">
            file_download
         </span>
      </a>
      <ng-template #downloadTemplate>
         <span>Download this file</span>
      </ng-template>
   </div>
</div>
```