---
title: List of changes
lang: en-EN
---

# General todo list

+ Sitemap creation (where to put in the front?)
+ Déclaration d'accessibilité (?)
+ 
+ 

# List of changes
## General and Homepage

+ Collapsible sections contrast fixes and icons
+ Add aria-labels to sliding keywords component + adding **role search** to the filters
+ alt attributes on logo and images

### Navigation for carousels

We had a contrast issue in here. We then updated the CSS, removing the gray background and replacing dark gray font icons with full black SVGs. 
We also updated the aria-attributes and roles of the slide container, the slides, the content, etc..
The rest of the work to be done on carousels is very specific and should be checked by a developer.

### Nested links removed in events

While working on accessibility, we have to inspect each line of code and found in event cards an error. There were an empty link inside the link of the card. Obviously, we should avoid having nested links (link in a link). 

### Keywords

The keywords badges have been updated, removing the light gray background and replacing it by a full black border.

+ Keywords (contrasts and aria-attributes)
+ Colors (lost of keywords)
+ Download buttons
+ Replacing all icons
+ Header and menus
+ Breadcrumbs
+ Footer
+ Social links
+ Removed all help-icons in forms with SVG and fix contrast
+ Contrast and visibility improved for the lad more buttons

## Calendar Landing page

+ Nested links removed
+ Reoder search form to be more logical
+ Fixing labels linking them with the **epID** attribute, instead of **id**
+ Print button replaced (icon removed and replaced by text content)
+ Main icon set as role="presentation" (also in cards)

## Legislative database

+ Add **epId=""** to form element coming from MCC and **for=""** attribute to the related labels. Other form element can have regular **id** and **for** attributes.
+ Question: How to manage id and labels when we have checkboxes in labels (MCC) in legislative database (scrutiny events)

Will be fixed with search section.

## Document

+ **Updating the deadline display**. It was red and had not enough contrast. Somution is to set the font color to black, and display a blinking red element in front to catch focus of the users.
+ **Replacing H5 by H3** in classification and "other files" sections to get the right hierarchy.
+ Adding a **sr-only** span to the "edit" link (for logged in users)

# Proposed changes

+ Change the font to the [Atkinson Hyperlegible](https://brailleinstitute.org/freefont), from the Braille Institute


## Footer

Example with the icon (go to top)