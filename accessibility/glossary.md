---
title: Glossary
lang: en-EN
---

+ Assistive technologies 
+ User agent
+ **WAI** : Web accessibility initiative
+ **ARIA**: Accessible Rich INternet Applications
+ [WCAG](https://www.w3.org/WAI/standards-guidelines/wcag/): Web Content Accessibility Guidelines
+ [ATAG](https://www.w3.org/WAI/standards-guidelines/atag/): Authoring Tool Accessibility Guidelines

## Dispatching the work


### ALL

+ Translations via this file :
### Federica

+ Everything related to the keyboard navigation
+ The navigation order in the page
+ The VoiceOver analysis
+ Defining what is missing and what should be changed
+ Providing a full list of changes to implement

### Nicolas

+ Everything related to colors, contrasts ans graphical solutions to these issues.
+ Typography and Reflow
+ Icons
+ Adding the right aria-label, sr-only classes, and roles to html elements. This is related with what will provide Federica.

### Majdi

Performance issues and everything related to the Backend and API calls

### Sydnei / Oscar

+ Front-end work, particularly on Angular components and everything related to the MCC library.
