---
title: Tools and ressources
lang: en-EN
---

# Tools and ressources

A lot of tools are available in order to audit and improve web applications for accessibility.

Site Improve will be the one used to audit IPEX and get our final score per template. However it has to be run by DGCOMM (to check) and only then we wil get the report with remaining issues and score.

As accessibility is an ongoing work and templates have to be continuously audited, it is important for the team to be able to audit pages using alternative tools, with the particularity that the DEV environment is not accessible from outside the EP network.

These tools and ressources seem to be very useful and practical. SOme of them are coming from the [W3C list of axe tools](https://www.w3.org/WAI/ER/tools/) ,some from other places. These should be enough to be able to audit our templates and get a idea of the score we may get from Site Improve.

## General

+ [A customizable quick reference to Web Content Accessibility Guidelines (WCAG) 2 requirements (success criteria) and techniques](https://www.w3.org/WAI/WCAG21/quickref/)
+ Should we provide an accessibility status page? (as [in Digital wallonia](https://www.digitalwallonia.be/fr/declaration-daccessibilite))

## Slides

+ [MCC Accessibility slides](#)
+ [Anysurfer slides](https://slides.anysurfer.be/reveal/tech_en2021.html#/)
+ **Stéphanie Walter**'s slide for AXE CON 2022 about [Documenting Accessibility & user interactions](https://www.deque.com/axe-con/wp-content/uploads/2021/12/accessibility-documentation-for-designers.pdf)

## Audit tools

+ The [BOSA Accessibility Check](https://accessibility.belgium.be/fr/tools/bosa-accessibility-check) is a software that can be set as a browser bookmark and generate an accessibility report on the visited page. You can then export the report. 
+ [Alix](http://ffoodd.github.io/a11y.css/)
+ [Orange open source tool](https://github.com/Orange-OpenSource/a11ygato-platform)
+ [Accessi.org](https://www.accessi.org/): full report with dos and donts
+ [Usablenet](https://usablenet.com/automated-accessibility-testing-tool#testingtoolform): A very powerful tool with clean and clear output report. Exists in Premium version with Jenkis/Jira integration and automated and scheduled audits.
+ And of course **Lighthouse in Chrome**, and **Firefox dev tools**.

Note that **Firefox dev tools allows to see the tabulation order**.

A lot of tools can be found in this [Web Accessibility Evaluation Tools List](https://www.w3.org/WAI/ER/tools/)

## Other ressources

+ [Lighthouse Accessibility](https://web.dev/lighthouse-accessibility/) - Excellent and very complete ressource
+ [Report template for accessibility (W3C)](https://www.w3.org/WAI/test-evaluate/report-template/)
+ [Report tool (W3C)](https://www.w3.org/WAI/eval/report-tool/)
+ [ATAG report tool](https://www.w3.org/WAI/atag/report-tool/about)
+ [Material design accessibility guidelines](https://material.io/design/usability/accessibility.html)
+ [Accessibility: sr-only or aria-label?](https://stackoverflow.com/questions/39223952/accessibility-sr-only-or-aria-label)
+ [sr-only or aria-label : some cases](https://accessible360.com/accessible360-blog/use-aria-label-screen-reader-text/)

## Specific tools for contrast and other visual issues

+ [Color Contrast Accessibility Validator](https://color.a11y.com/Contrast/)

## Training/learning

+ [https://learning.edx.org/course/course-v1:W3Cx+WAI0.1x+3T2019/home](https://learning.edx.org/course/course-v1:W3Cx+WAI0.1x+3T2019/home)