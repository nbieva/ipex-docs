---
title: IPEX Templates
lang: en-EN
---

# List of templates

List of templates to be audited in IPEX. Having those templates reaching the score of 98.5% will give a goood confidence about IPEX accessibility level.

+ **High Level elements**
    + Header
    + Logo
    + User and language menus - **ANGULAR**
+ [**Homepage**](https://secure.ipex.eu/IPEXL-WEB/) - Carousels, video, calendar, subscriptions - **Lighthouse: 100%**
    + Carousels - **ANGULAR**
    + Cards
    + Datepicker Calendar - **ANGULAR**
+ [**Calendar landing page**](https://secure.ipex.eu/IPEXL-WEB/calendar) - Search form, cards - **Lighthouse: 100%**
    + Carousel - **ANGULAR**
    + Search form - **ANGULAR**
    + Filters
    + Load more - **ANGULAR**
+ [**Calendar details 1**](https://secure.ipex.eu/IPEXL-WEB/calendar/082d29087c6ec48a017c7ec825330424) - **Lighthouse: 100%**
    + horizontal navigation
    + list of files
    + list of links
+ [**Calendar details 2**](https://secure.ipex.eu/IPEXL-WEB/calendar/082d29087bc011cf017bc98c683f0046)
    + horizontal navigation
    + list of files
    + list of links
+ [**Document landing page**](https://secure.ipex.eu/IPEXL-WEB/document) - **Lighthouse: 100%**
    + Regular list of cards
+ [**Document layout**](https://secure.ipex.eu/IPEXL-WEB/document/COM-2016-0128) - **Lighthouse: 100%**
    + Complex layout with timeline and files - **ANGULAR**
+ [**Scrutiny layout**](https://secure.ipex.eu/IPEXL-WEB/document/COM-2016-0128/czsen)
    + Complex layout with timeline and files - **ANGULAR**
+ [**Legislative database**](https://secure.ipex.eu/IPEXL-WEB/document/eulegislativedatabase)
    + Documents search form
+ [**High activity**](https://secure.ipex.eu/IPEXL-WEB/document/highActivity)
    + List of complex cards with timelines. Accessibility work will be already done on other template like documents layout and landing pages.
+ [**Static pages**](https://secure.ipex.eu/IPEXL-WEB/parliaments/static/8a8639977b0a853a017b10723321004b)
    + Specific content from the Quill Editor (user)
+ [List of parliaments](https://secure.ipex.eu/IPEXL-WEB/parliaments/list_parliaments)
    + List of cards with different chambers
+ [Parliament details page](https://secure.ipex.eu/IPEXL-WEB/parliaments/list_parliaments/debta)
    + Complex page
    + Lists of links can take some time
+ [List of news](https://secure.ipex.eu/IPEXL-WEB/parliaments/news_parliaments) - List of cards with search form
+ [News details page](https://secure.ipex.eu/IPEXL-WEB/parliaments/news_parliaments/082d29087e7b97c0017ea06568d0026f) - Editorial content
+ [Conference layout - Home tab](https://secure.ipex.eu/IPEXL-WEB/conferences/cosac/home) - Subnavigation + sections + lists of files
+ [Conference layout - Meetings tab](https://secure.ipex.eu/IPEXL-WEB/conferences/cosac/meetings) - Subnavigation + lists of meetings
+ [Conference layout - About tab](https://secure.ipex.eu/IPEXL-WEB/conferences/eu_speakers/about) - Subnavigation + editoorial content + Media gallery
+ [About - IPEX Board](https://secure.ipex.eu/IPEXL-WEB/about/ipex_board) - List of contact cards
+ [About - Secretaries general](https://secure.ipex.eu/IPEXL-WEB/about/secretaries_general) - List of contact cards, by country and chamber
+ [My IPEX - Login](https://secure.ipex.eu/IPEXL-WEB/user/login) - Form
+ [My IPEX - Create an account](https://secure.ipex.eu/IPEXL-WEB/user/createAccount) - Form

Other templates will come for the **IPEX Search section**.
