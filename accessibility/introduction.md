---
title: Notes on accessibility
lang: en-EN
---

# Notes on accessibility

> We cannot check all accessibility aspects automatically. **Human judgement is required.** Sometimes evaluation tools can produce false or misleading results. Web accessibility evaluation tools can not determine accessibility, they can only assist in doing so. [Source](https://www.w3.org/WAI/test-evaluate/tools/selecting/#cannot)

> Knowledgeable human evaluation is required to determine if a site is accessible. [Source](https://www.w3.org/WAI/test-evaluate/)*

These guidelines aim to provide some milestones and tips in this very vast domain. For most of us, web accessibility is new. We know the main principles but having IPEX fully accessible will force us to dive deep in the analysis of every single component and evaluate for each one the actions to take. This task will imply each and everyone of us: project managers, back-end and front-end developers, designers, testers, as well as the business users, as this concerns also the content itself.

Obviously, adressing accessibility issues will have an impact on the front office in terms of design and layout. Developers and business users have to keep in mind that improving IPEX accessibility is valuable to all, not only disabled people. In this spirit, we assume that accessibility is prior and has a higher value than pure visual design. Our choices have to be made accordingly.

Also, as we will need to define a lot of content elements (texts that, sometimes, are still static), we have to implement them in a dynamic way in the application, add them to the label manager with a predefined pattern to follow so can can be translated by the different IPEX correspondents for each Parliament.

To do so, a shared spreadsheet is available here: [https://docs.google.com/spreadsheets/d/1NFcbPO7hYIBdqlJAux5RbFjOry7q4llu52eFXkf7798/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1NFcbPO7hYIBdqlJAux5RbFjOry7q4llu52eFXkf7798/edit?usp=sharing)

In this spreadsheet, please list each and every content added to IPEX (dont forget the **translate pipe** when you implement in the application) along with its location, type and label key.

Accessibility is a team work and will be achieved only if we all work in a proactive way. Please refer to the W3C docs or ask another team member to get answers to your questions.

## Goal

We have 2 goals to reach:

1. The **automatic aspect**: Get at least 98.5% of accessibility score on Site Improve report (from DGCOMM) on every [template provided](templates). **These reports will only be possible once in Production.**
2. The **human factor**: Having a "Real-life" green light from Tanja (or another accessibility expert) if possible.

## Scope

Accessibility was a requirement since the very beginning of the project. It is very important to have it done well.
This accessibility audit and fixes will be oparated on the Front-office only and will focus and major modern browsers.

[https://www.w3.org/WAI/eval/report-tool/evaluation/define-scope](https://www.w3.org/WAI/eval/report-tool/evaluation/define-scope)

## Proposed methodology

As said above, web accessibility is a huge task involving a lots of skills. In order to stay on the right path, we should use a progressive methodology, addressing the different aspects in a specific order, from the more general to the more specific, taking into consideration the tools we have at our disposal and the specific context of the European Parliament. 

As Site Improve reports will not be available before production, we have to review our first planning. Proposed steps:

1. **Analysis and first review** on some representative templates
2. **Lighthouse tests and fixes** > 100%
3. **Deque Axe reports** collection (**All templates**) + making them understandable.
4. **Design fixes, ARIA-attributes and roles**
5. **Angular components and MCC library** fixes
6. **Tanja or other expert review**
7. **Final reports and validation**
8. **Preproduction** deployment
9. **Site Improve** first audits

Steps 4 and 5 are intricated and will most probably be adressed at the same period of time.
## Top tools

As Site Improve reports are unavailable until Production release, we have to find other tools to use until then:

+ The [**Axe Dev Tools by Deque**](https://www.deque.com/axe/devtools/) are very powerful and precise. They have a 15 days trial that we can use.
+ The [**BOSA Accessibility Check**](https://accessibility.belgium.be/fr/tools/bosa-accessibility-check) is also a good resource to audit pages.

You will find more tools and resources on [this page](tools).

## General links and documents

+ The **new** [Web Content Accessibility Guidelines](https://www.w3.org/TR/WCAG21/) (WCAG) 2.1, level AA
+ [Accessible Rich Internet Applications](https://www.w3.org/TR/wai-aria-1.1/) (WAI-ARIA) 1.1
+ [European Standard on accessibility requirements for public procurement of ICT products and services EN 301 549](https://www.etsi.org/deliver/etsi_en/301500_301599/301549/03.02.01_60/en_301549v030201p.pdf).
+ [Directive (EU) 2016/2102](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32016L2102) of the European Parliament and of the Council of 26 October 2016 on the accessibility of the websites and mobile applications of public sector bodies (Text with EEA relevance )


### To check

+ **Typography** and **reflow**
+ **Colors** and **contrasts**
+ **Icons** system
+ Sections and **roles**
+ **Aria attributes**
+ **Keyboard navigation**
+ Specific behaviour related to the **use of Angular**
+ Automatic **captions on videos**



