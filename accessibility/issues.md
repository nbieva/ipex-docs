---
title: Issues to be addressed
lang: en-EN
---

This page lists the main issues to be addressed in order to improve accessibility. The following is a summary to get the big picture.

## General

+ Clean the code
+ Providing a **site map** is very important. We should discuss where to place it. This is important for the users and visitors, not only for SEO.
+ Systematically manage translations (no static content!)
+ [Atkinson Hyperlegible Font](https://brailleinstitute.org/freefont) also on [Google fonts](https://fonts.google.com/specimen/Atkinson+Hyperlegible)
+ Sometimes, audit tools like Lighthouse will say that "Document doesn't have a **title** element". This is because the audit is done before the dynamix rendering is finished. At the end all pages have the title element. (See in the browser tab)

## Content

Accessibility is also about content itself. We may then, at the end, provide a very simple guide for the user to help them create accessible editorial content.

## Translations

As the Accessibility effort will go into code at a detailed level and will generate a lot of labels and descriptions, we should list all these in a document in order to add them to the labels in the backoffice label manager.

[This spreadsheet](https://docs.google.com/spreadsheets/d/1NFcbPO7hYIBdqlJAux5RbFjOry7q4llu52eFXkf7798/edit?usp=sharing) can be used at this stage. (Ask for access)

## Typography and Reflow

Website should use a font collection that focus on readability. The entire application should support any font size change decided by the user on the browser side. All information should remain accessible.

## Colors and contrasts

Color, contrasts and other visual accessibility fixes will improve IPEX accessibility for both regular users and people with disabilities. Those changes will bring some visual adaptations.

## Semantic HTML

+ H1, H2, H3, .. Hierarchical and logical markup will improve accessibility. **Please be aware that any change in the HTML markup may require a CSS update!** Please test after any change.
## Keyboard navigation

For each template/page, we should test with VoiceOver (or other) + define an order for the page to be read with a screen reader or keyboard. This can be implemented using tab-order attribute.
See Federica's document for more information.

## sr-only

The **sr-only class** is used to place content that will only be available to screen readers. We may then use it also to provide specific guidance to screen reader users, or to manage to way an item may be read to make it more clear. For example :

```html
<p class="ipx-card-description">
    <span class="sr-only">From May 5th 2022 to May 30th 2022</span>
    27/05/2022-30/05/2022
</p>
```

Please note that all these "hidden" contents have to be translated as well.

+ [https://stackoverflow.com/questions/39223952/accessibility-sr-only-or-aria-label](https://stackoverflow.com/questions/39223952/accessibility-sr-only-or-aria-label)
+ [https://stackoverflow.com/questions/15883778/pausing-in-a-screen-reader-for-accessibility/55273574#55273574(https://stackoverflow.com/questions/15883778/pausing-in-a-screen-reader-for-accessibility/55273574#55273574)]
+ [https://a11y-guidelines.orange.com/en/web/components-examples/accessible-hiding/](https://a11y-guidelines.orange.com/en/web/components-examples/accessible-hiding/)

## Forms

+ Every form element has to be associated with a **label**. There are [2 ways to add labels](https://web.dev/label/) to form elements. However, only the second one (wrap the element within the label) seems to pass the Lighthouse audit (1st solution still gives an error).
+ In fact we should use **epId** attribute and use first solution. The form actions are manage in the typescript files.
+ **ISSUE:** In Documents search form, the date picker (from to) have the epId attribute. But the library put that id on the input AND the container of the input. This triggers Warnings in lighthouse and takes down the score.

## Roles

Roles are HTML attributes that can be added to any HTML element to inform the users and screen readers about their role in the page. Note that there are HTML elements taht already has implicit roles. For example the **nav** element has a role of navigation.

+ About roles: [https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles)
+ **Section** role: Too abstract. Do not use.
+ link role is implicit on **a** elements. No need to add it.
+ Use the nav element (no need for role on nav) The element automatically communicates that a section has a role of navigation.
+ We should use the **role="list"** on all list of cards that are not actual **ul** elements. For example a carousel. List items should have the “listitem” role, even if they are not **li** elements.

More info about Carousels can be found here: [https://www.w3.org/TR/wai-aria-practices-1.1/examples/carousel/carousel-1.html#](https://www.w3.org/TR/wai-aria-practices-1.1/examples/carousel/carousel-1.html#)

## Aria attributes

+ **aria-label** attribute is very important. This attribute will replace the content of an element. If you want to contextualize an action or a section, you may want to use the **sr-only** class (on a span) that will read that before or after the actual HTML content (not replacing it). This may be useful to comment some parts or provide indications about the content, like a user guide. The sr-only class, obviously, is only rendered by screen-readers.
+ Check **aria-expanded** everywhere. It seems to work on ant-collapse components
+ **aria-haspopup** attribute has to be set to true on an element that will trigger an action like deploying a dropdown list (see *My IPEX* menu)
+ If aria-labels are set for a div, that div should have the correct role. aria-labels are not well supported on a div with no role.

## Links

All links and interactive elements should be clearly identifiable and keyboard-focusable.

## Images

All image should have an **alt** attribute with a clear description. This description should not be too long but describe well the image.

For example, "Landscape" is not good. It should be, for example, "Some green hills with sheeps, with a beautiful sunset."

Also, it is important to process all images that can be uploaded by the users (news, parliaments, galleries...) to resize, compress or save them in other formats like AVIF and Webp. We could the use a picture html element to load the optimal format/size according to the context (desktop, browser) and the browser user (explorer, chrome..).
This could be implemented in Galleries.

```html
<picture>
    <source srcset="/images/monimage.avif">
    <source srcset="/images/monimage.webp">
    <img src="/images/monimage.jpg" alt="Mon image" />
</picture>
```

We may also have different versions of an image to be loaded according to the user's context:

```html
<picture>
    <source media="(min-width: 900px)" srcset="/images/monimage-1200px.jpg">
    <source media="(min-width: 600px)" srcset="/images/monimage-900px.jpg">
    <img src="/images/monimage-600px.jpg" alt="Mon image" />
</picture>
```

## Icons

All icons should be replaced with regular SVGs. For accessibility, icon fonts are not the right solution, even if it is very practical. An icon font displays content that is not real content.

```html
<span class="material-icons">keyboard_arrow_right</span>
```

It is preferrable to use regular SVGs (in the code, or as background-images), along with an associated aria-label.

```html
<a class="btn" title="Send an email" href="mailto:example@example.com" aria-label="Send an email to example@example.com">
    <span class="material-icons mail"></span>
</a>
```

+ [https://www.irigoyen.dev/blog/2021/02/17/stop-using-icon-fonts/](https://www.irigoyen.dev/blog/2021/02/17/stop-using-icon-fonts/)

## Videos and transcripts

Captions should be displayed by default on all videos.

For youtube, this can be achieved by adding **?cc_load_policy=1&cc_lang_pref=en** to the end of the URL reference on the iframe code.

However, this seems not to work with automatic captions, so captions/transcript has to be added on Youtube side.

## Specific issues to Angular and other libraries

+ Some components are inaccessible in html files, like **ngx-gallery**. We have to make those components accessible and keyboard-focusable.
+ **Scrollable element** is not keyboard accessible (keywords) - Check other places with Perfect scrollbar library
+ **My IPEX and Language**. list are inaccessible to develop with Keyboard : **check all drropdowns**
+ When we do a **load more on a list** (of events,  on calendar page for example), focus should not be lost and tabbing should continue scroll down the list.
+ **Main navigation** has to be revised.
+ Fix **ep-collapse** and add role in component
+ In Documents search form, the **date picker** (from to) have the epId attribute. But the library put that id on the input AND the container of the input. This triggers Warnings in lighthouse and takes down the score.
+ **Back to top** component is not working
+ Carousel Previous and Next navigation elements have to be updates in the typescript file.
+ Collapsible sections can be accessed with keyboard, even if it is collapsed.
+ Carousel navigation (in homepage for example) is not keyboard focusable. We should test and fix.
+ The **events dates format for screenreaders** have to be changed. define also a way to get translations for this. See below:

It can be for example:

```html
<p class="ipx-card-description">
    <span class="sr-only">From May 5th 2022 to May 30th 2022</span>
    27/05/2022-30/05/2022
</p>
```

+ Adding dynamic Video description **(todo)**


## Performances

We should also address performance issues.