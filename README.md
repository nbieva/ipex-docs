---
title: Notes on accessibility
lang: en-EN
---

# Notes on accessibility

We cannot check all accessibility aspects automatically. **Human judgement is required.** Sometimes evaluation tools can produce false or misleading results. Web accessibility evaluation tools can not determine accessibility, they can only assist in doing so.

[Source](https://www.w3.org/WAI/test-evaluate/tools/selecting/#cannot)

Knowledgeable human evaluation is required to determine if a site is accessible. [Source](https://www.w3.org/WAI/test-evaluate/)*


## General links and documents

+ Web Content Accessibility Guidelines (WCAG) 2.0, level AA
+ the new Web Content Accessibility Guidelines (WCAG) 2.1, level AA
+ Accessible Rich Internet Applications (WAI-ARIA) 1.0
+ Accessible Rich Internet Applications (WAI-ARIA) 1.1
+ European Standard on accessibility requirements for public procurement of ICT
products and services EN 301 549.
+ [Directive (EU) 2016/2102](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32016L2102)
Directive (EU) 2016/2102 of the European Parliament and of the Council of 26 October 2016 on the accessibility of the websites and mobile applications of public sector bodies (Text with EEA relevance )


### To check

+ **Typography** and **reflow**
+ **Colors** and **contrasts**
+ **Icons** system
+ Sections and **roles**
+ **Aria labels**
+ **Keyboard navigation**
+ Specific behaviour related to the **use of Angular**
+ Automatic **captions on videos**

## In IPEX

Accessibility was a requirement since the very beginning of the project.
This accessibility audit and fixes will be oparated on the Front-office only.
The difficulty with accessibility is that every line of code is a question. And having to manage this at this stage of the project is also an issue. It should have been done since the beginning.