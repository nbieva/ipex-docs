---
title: Ant Selects
lang: fr-FR
---

# Ant Selects

<p class="lead">As for other ant components, the Ant Select component generates additional markup in the HTML. These have been styled to match the IPEX interface. These Ant-design select should be used (and systematically tested) everywhere we have select boxes in IPEX for more functional and graphical consistency.</p>

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/home/">Demo</a> (this page is just for graphical testing)

Ant Design docs may be found here: [https://ant.design/components/select/](https://ant.design/components/select/)

This has to be added to the Ant design select component implementation, in order not to impact the back office design --> **dropdownClassName="ant-custom"**

LESS files need of course to be updated.

<a data-fancybox="editor" title="" href="/assets/calendar-filters.jpg">![](/assets/calendar-filters.jpg)</a>
<a data-fancybox="editor" title="" href="/assets/homefilter-01.jpg">![](/assets/homefilter-01.jpg)</a>
<a data-fancybox="editor" title="" href="/assets/homefilter-02.jpg">![](/assets/homefilter-02.jpg)</a>

::: tip
Please note that the only way I can style and test this is through my test Nuxt application which is built on Vue.js. I used ant-design-vue to implement the components in the application before styling. I guess we may find some differences or inconsistencies when implementing this in the Angular application. So please **double check the screenshots** that give you a very precise idea of how it is supposed to be displayed. **Things have to be displayed like that.**
:::

## Markup used in the Nuxt app

This is the markup I used in the Nuxt application, with ant-options. This was used to filter dynamically institutions list in the News carousel on the homepage. (same in Calendar of events page). The dropdownClassName **ant-custom** is very important here in order to customize the styles with no impact on the backoffice.

### In Calendar of events page

```html
<!-- Calendar page filter -->
<a-select
    class="filter-input"
    show-search
    dropdownClassName="ant-custom"
    default-value="All institutions"
    option-filter-prop="children"
    :filter-option="filterOption"
    @focus="handleFocus"
    @blur="handleBlur"
    @change="handleChange"
>
    <a-select-option value="All institutions">
        All institutions
    </a-select-option>
    <a-select-option value="Austrian Parliament (AT)">
        Austrian Parliament (AT)
    </a-select-option>
    <a-select-option value="Chambre des Représentants (BE)">
        Chambre des Représentants (BE)
    </a-select-option>
    <a-select-option value="Deutscher Bundestag (DE)">
        Deutscher Bundestag (DE)
    </a-select-option>
    <a-select-option value="European Commission">
        European Commission
    </a-select-option>
    <!-- Other options of the list... -->
</a-select>

```

### For a basic select

<a data-fancybox="editor" title="" href="/assets/homefilter-03.jpg">![](/assets/homefilter-03.jpg)</a>

```html
<a-select
    class="filter-input"
    default-value="Option one"
    option-filter-prop="children"
    @blur="handleBlur"
    @change="handleChange"
>
    <a-select-option value="Option one">
        Sort by option one
    </a-select-option>
    <a-select-option value="Option two">
        Sort by option two
    </a-select-option>
    <a-select-option value="Option three">
        Sort by option three
    </a-select-option>
    <a-select-option value="Option four">
        Sort by option four
    </a-select-option>
</a-select>
```