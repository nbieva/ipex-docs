---
title: Ant Carousel
lang: fr-FR
---

# Ant Carousel

<p class="lead">As for other ant components, the Ant Carousel component generates additional markup in the HTML. These have been styled to match the IPEX interface.</p>

<a data-fancybox="editor" title="" href="/assets/carousel-01.png">![](/assets/carousel-01.png)</a>

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/conferences/themes-base/">Demo</a> (this page is just for graphical testing)

You should add 2 additional classes to the carousel element:  **custom-carousel** and **custom-gallery-carousel**.

Carousel uses the [fancybox plugin](https://fancyapps.com/fancybox/3/) to enlarge images on click, like many other images in IPEX. There should be [a version of this for Angular](https://www.npmjs.com/package/angular-fancybox-plus)(to be tested). This should be implemented in teh Angular project of course. **Every image should come in 2 different formats/dimensions**. One at a thumbnail size (like 400px width) that is displayed in the carousel, and one at full size (like 1200 or 1400px width) that will be displayed when enlarged (after clicking on the thumbnail).

There is no large image for the default image, as it does not have to be enlarged.

The Carousel displays groups of 4 images. When there is no image, a default gray sample is displayed to keep the layout consistent (see below).

<a data-fancybox="editor" title="" href="/assets/carousel-02.png">![](/assets/carousel-02.png)</a>

Images are displayed in one row and rearranged in 2 rows of two images on mobile devices.

<a data-fancybox="editor" title="" href="/assets/carousel-05.png">![](/assets/carousel-05.png)</a>

::: tip
Please note that the only way I can style and test this is through my test Nuxt application which is built on Vue.js. I used ant-design-vue to implement the components in the application before styling. I guess we may find some differences or inconsistencies when implementing this in the Angular application. So please **double check the screenshots** that give you a very precise idea of how it is supposed to be displayed. **Things have to be displayed like that.**
:::

## Markup used in the Nuxt app

The wrapper tag will be different I guess but the inner contents should be those ones. Of course, hypertexte references and image attributes (alt) have to be updated.

```html
<a-carousel arrows class="custom-carousel custom-gallery-carousel" :afterChange="onChange">
    <div slot="prevArrow" slot-scope="props" class="custom-slick-arrow" style="left: 10px;zIndex: 1">
        <a-icon type="left-circle" />
    </div>
    <div slot="nextArrow" slot-scope="props" class="custom-slick-arrow" style="right: 10px">
        <a-icon type="right-circle" />
    </div>
    <div>
        <figure class="img-wrapper">
            <a data-fancybox="conference-gallery" href="/img/conference/demo/01.jpg">
                <img src="/img/conference/demo/01-thumb.jpg" alt="Sample image">
            </a>
        </figure>
        <figure class="img-wrapper">
            <a data-fancybox="conference-gallery" href="/img/conference/demo/02.jpg">
                <img src="/img/conference/demo/02-thumb.jpg" alt="Sample image">
            </a>
        </figure>
        <figure class="img-wrapper">
            <a data-fancybox="conference-gallery" href="/img/conference/demo/03.jpg">
                <img src="/img/conference/demo/03-thumb.jpg" alt="Sample image">
            </a>
        </figure>
        <figure class="img-wrapper">
            <a data-fancybox="conference-gallery" href="/img/conference/demo/04.jpg">
                <img src="/img/conference/demo/04-thumb.jpg" alt="Sample image">
            </a>
        </figure>
    </div>
    <div>
        <figure class="img-wrapper">
            <a data-fancybox="conference-gallery" href="/img/conference/demo/05.jpg">
                <img src="/img/conference/demo/05-thumb.jpg" alt="Sample image">
            </a>
        </figure>
        <figure class="img-wrapper">
            <a data-fancybox="conference-gallery" href="/img/conference/demo/06.jpg">
                <img src="/img/conference/demo/06-thumb.jpg" alt="Sample image">
            </a>
        </figure>
        <figure class="img-wrapper">
            <a data-fancybox="conference-gallery" href="/img/conference/demo/07.jpg">
                <img src="/img/conference/demo/07-thumb.jpg" alt="Sample image">
            </a>
        </figure>
        <figure class="img-wrapper">
            <a data-fancybox="conference-gallery" href="/img/conference/demo/08.jpg">
                <img src="/img/conference/demo/08-thumb.jpg" alt="Sample image">
            </a>
        </figure>
    </div>
    <div>
        <figure class="img-wrapper">
            <a data-fancybox="conference-gallery" href="/img/conference/demo/09.jpg">
                <img src="/img/conference/demo/09-thumb.jpg" alt="Sample image">
            </a>
        </figure>
        <figure class="img-wrapper default">
            <img src="/img/conference/demo/sample-gallery-default-thumb.jpg" alt="Sample image">
        </figure>
        <figure class="img-wrapper default">
            <img src="/img/conference/demo/sample-gallery-default-thumb.jpg" alt="Sample image">
        </figure>
        <figure class="img-wrapper default">
            <img src="/img/conference/demo/sample-gallery-default-thumb.jpg" alt="Sample image">
        </figure>
    </div>
    <div>
        <figure class="img-wrapper default">
            <img src="/img/conference/demo/sample-gallery-default.jpg" alt="Sample image">
        </figure>
        <figure class="img-wrapper default">
            <img src="/img/conference/demo/sample-gallery-default.jpg" alt="Sample image">
        </figure>
        <figure class="img-wrapper default">
            <img src="/img/conference/demo/sample-gallery-default.jpg" alt="Sample image">
        </figure>
        <figure class="img-wrapper default">
            <img src="/img/conference/demo/sample-gallery-default.jpg" alt="Sample image">
        </figure>
    </div>
</a-carousel>
```
No additional methods or options have been added.

## Styles in the less file (for info)

```less
.custom-carousel {
  &.ant-carousel {
    margin-bottom:3rem;
    .custom-slick-arrow {
      width: 36px;
      height: 36px;
      font-size: 36px;
      color: #fff;
      background-color: #222;
      background-color: @basedark;
      border-radius:60px;
      opacity: .6;
      transition: all ease .2s;
      border:none;
      &:before {
        display: none;
      }
      &:hover {
        opacity: 1;
      }
    }
    .slick-next {
      right:-18px !important;
    }
    .slick-prev {
      left:-18px !important;
    }
    .slick-dots {
      display:none !important;
    }
    &.custom-gallery-carousel {
      .slick-slide {
        div {
          div {
            display:flex !important;
            justify-content: space-between;
            align-items:center;
            flex-wrap:wrap;
            .img-wrapper {
              width:calc(25% - .75rem);
              max-height:150px;
              border-radius:4px;
              margin-top:.75rem;
              margin-bottom: 0;
              background:@base;
              overflow:hidden;
              &.default {
                background:none;
                img {
                  opacity:1;
                  &:hover {
                    cursor:default;
                  }
                }
              }
              @media @md-down {
                width:calc(50% - .375rem);
                max-height:200px;
              }
              @media @sm-down {
                width:calc(50% - .375rem);
                max-height:150px;
              }
              img {
                opacity:.8;
                object-fit: cover;
                object-position: 50% 50%;
                border-radius:4px;
                transition: all ease .2s;
                &:hover {
                  opacity:1;
                  cursor:pointer;
                }
              }
            }
          }
        }
      }
    }
  }
}
```