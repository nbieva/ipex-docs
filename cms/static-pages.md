---
title: Manage sections and pages
lang: fr-FR
---
IPEXL-2054 - Section CMS documentation
Difficulty in here: There are many points in the Section CMS that are not as they should be, so I had to correct some screens. So the developers will have something to based on. That has definitely to be corrected.

### Advantages for the web docs:

+ Texts flows without having to take care about layout to be edited in additional sowtwares loke Adobe XD, Sketch or Powerpoint.
+ Living and flexible project
+ Techno all developers and designers should be able to use.
+ Git versioning
+ Markdown editing
+ Code snippets right in the page (can be copy-pasted)
+ Can use absolute or relative links of display external living contents
+ Can be hosted within the project itself
+ Statically generated
+ So can be used offline as well.
+ Can have several people working on the documentation at the same time
+ eBook exportable
+ Styled as a website. So entire doc can be visually updated in one go.
+ You put the content you want regardless of the format

# Sections CMS

<p class="lead">The section CMS is the tool to use if you want to create your own custom pages with free text and add them to the main IPEX navigation. As other administration tools, you will find it in the Back-office of IPEX, once logged in the application. On the Back-office sidebar, find CMS and then Section CMS.</p>

CMS (Content Management System) application is answering the request of the board to be able to use a set of related tools that will be used to create and manage IPEX content without needs of a technical person.


### Goals and objectives 

+ CMS application replaces the most of the widgets made for the V2 of IPEX.  
+ The IPEX V2 widgets meant to edit some sections of predefined pages by changing the content or by linking modules of applications assigned to specific area of the page.
+ CMS module has functions such as modifying the main structure located in the back office and a possibility for edition functions to be accessible from the front end.
+ The multilingual module should handle the translations of contents.
+ Rich Editor Module, integrated gallery and Files Manager




First of all, you will need, of course, to have your content ready. You can use, for this, your prefered text editor.
However, to avoid issues while converting formatted text, you should prefer to use un-formatted text editor like Notepad and save your work as a .txt or .rtf file.
Do not style your content at this stage. This will be done in the IPEX editor.
Each new page you publish will appear in the main menu and on the landing page of the concerned section. 

### The Section CMS can manage:

+ The **section level** (Section title, section description, section icon..)
+ The **page level** (Page title, page description, page icon/featured image..)

<a data-fancybox="sectionscms" title="" href="/assets/sections-cms-01.jpg">![](/assets/sections-cms-01.jpg)</a>

## Sections

Represent the main sections of IPEX. Accessible from the top navigation of IPEX website. Default Sections are Calendar, Documents, Parliaments, Conferences and About.

If you navigate to the Section CMS, you will find a collapsible list of sections. **Each section may contain several pages**, either dynamic or static.

No new section can be created.

In the front-office (the website itself), **each section has its landing page** that list all the pages contained in that section. That landing page is the exact reflection of the main menu (name of the pages, ranking..)

An exception is the Calendar page, which is at the same time a section and a page. There are no sub pages for this calendar section.

**Note that those Landing pages are very important, as they are the entry point to the subpages on mobile devices.**

Each Landing page's header displays the section title, featured image and description.

Each card within the Landing page's body displays the same kind of information but for each page in the section.


## Pages

### Dynamic pages

We call dynamic pages the pages with a content that may automatically change over time. For example, the *High Activity Documents* page is a dynamic page that will load different procedures each time you visit it. Other dynamic pages are Calendar page, News, Contacts, etc.. The content of those dynamic pages cannot be edited by the users.
Additional dynamic pages cannot be created.

### Static pages

Slightly different are the Static Pages. The Static pages **can be created and managed by the users**, with some parameters to be set (Description, featured image, etc..) and, of course, the content (body) of the page, which is a **free text** filled via the back office editor.

The creation/edition of those pages has to follow a specific workflow described below.

If a new page is created and published, it will appear in the IPEX main menu.

## Create your page

Obviously, the first thing to do is to create your page and gives the minimal infomation about it: The page title. You will be invited to enter that information i a popup window.

There are three different places from which you can create a new page:

1.  The **Create new page** link you will find at the bottom of the section's list of pages (List of sections)

<a data-fancybox="sectionscms" title="" href="/assets/create-new-page-00.jpg">![](/assets/create-new-page-00.jpg)</a>

2. The **Create new page** link you will find at the top right of the interface, while editing a page

<a data-fancybox="sectionscms" title="" href="/assets/create-new-page-01.jpg">![](/assets/create-new-page-01.jpg)</a>

3. The **Add new page** link you will find in the section dropdown menu. This will create a page within that section.

<a data-fancybox="sectionscms" title="" href="/assets/create-new-page-01b.jpg">![](/assets/create-new-page-01b.jpg)</a>

Click that **Create new page** button to create your page. You will then be invited, via a popup window, to enter the minimal infomation about your page: The page title.

<a data-fancybox="sectionscms" title="" href="/assets/create-new-page-02.jpg">![](/assets/create-new-page-02.jpg)</a>

Once done, your page will have a "DRAFT" status, and you will be redirected to the administration of your page in the Parent section interface.

For example, if you created a new page in the Document section, your page will be listed in the navigation tabs along with other pages of this section (Legislative database, EU Affairs documents ...)

## The page status

Once your page is created, you will see that it has a *Draft* status. You can keep that status while editing your page. It is understood as an "Incomplete" or "Work in progress" status. This is the default status set on creation.

<a data-fancybox="sectionscms" title="" href="/assets/create-new-page-03.jpg">![](/assets/create-new-page-03.jpg)</a>

To publish you page, you first need to pass it to the Hidden status. Once done, your page is there but is not visible. You will also notice that, once the status is set to Hidden, you have access to the Published status or the Archived status.

At this stage, you may want to publish your page.

From Published or Archived, you can only access the Hidden status. Indeed, you need to pass by that Hidden status in order to switch from Published to Archived, for example. 

<a data-fancybox="sectionscms" title="" href="/assets/pages-workflow-01.jpg">![](/assets/pages-workflow-01.jpg)</a>

## Editing your page

In this interface, you will be able to set or edit, for your page, the following informations:

### Texts

#### Title

Well, this was already given but you may want to change it at this stage. You can also set different titles for several languages.

#### Description

The description of your page is a brief summary giving indication about its content and information we may find in it. Try not to exceed 300 characters to avoid having a too heavy header for your page. 

As there will be no default description for your page, this is an important point to take care of. However, it is not mandatory to have one. If no description is set, an empty space will be displayed on the right of the image, which will not be visually satisfying.

<a data-fancybox="sectionscms" title="" href="/assets/process-texts.jpg">![](/assets/process-texts.jpg)</a>

### Ranking

The ranking will allow you to reorder the items in the main menu, but also in the section landing page (as they are both correlated).

<a data-fancybox="sectionscms" title="" href="/assets/process-ranking.jpg">![](/assets/process-ranking.jpg)</a>

<a data-fancybox="sectionscms" title="" href="/assets/process-ranking-2.jpg">![](/assets/process-ranking-2.jpg)</a>

### Featured image

The featured image is the image that will be displayed in the page header aside from the page description. Each image on IPEX should have an alternative text (caption) and a description. You may want to use the same content for both. Having those will improve accessibility and SEO.

A language can be assigned to each so the right translation can be loaded when changing the application's main language.

Please upload an image that has a reasonable definition, as we are on the context of the web and, furthermore a mobile web. Choose image that are minimum 1000px width (or height depending on orientation). prefer JPG format for pictures, and transparent PNGs for logos if needed.

If no icon/image is set for the page, a default one will be loaded (the one of the section in grayscale).

<a data-fancybox="sectionscms" title="" href="/assets/process-image.jpg">![](/assets/process-image.jpg)</a>

### Body

Here comes your page content. Choose a language by clicking one of the language buttons and start editing your page using the editor you will find below.

As it is a good practice not to have more than one H1 (heading level 1) within a page, prefer using H2 to structure your content.

Links you may want to add in your content should be formatted as absolute paths, beginning with **http** or **https** protocol. External links will be opened in a new browser tab.

You may also want to refer to contents within the IPEX application itself. To achieve this, you can grab the relative path to the page you want to link to.

For example: *parliaments/news_parliaments*

This will be opened in the same window.

<a data-fancybox="sectionscms" title="" href="/assets/process-body.jpg">![](/assets/process-body.jpg)</a>