---
title: CMS editor
lang: fr-FR
---

# CMS editor

<p class="lead">This is a proposal about the tools to use for front-end editing. This is part of the CMS work on IPEX.</p>

<a data-fancybox="editor" title="" href="/assets/editor-01.png">![](/assets/editor-01.png)</a>

<a class="btn-demo" target="_blank" href="https://ipextest.netlify.com/cms/editor">Demo</a>

+ [https://editorjs.io/](https://editorjs.io/)
+ [https://github.com/codex-team/editor.js/issues/716](https://github.com/codex-team/editor.js/issues/716)
+ [https://www.npmjs.com/package/vue-editor-js](https://www.npmjs.com/package/vue-editor-js)
+ Saving data: [https://editorjs.io/saving-data](https://editorjs.io/saving-data)

This uses [Editor.js](https://editorjs.io/) and needs to be configured.

<a data-fancybox="editor" title="" href="/assets/editor-02.png">![](/assets/editor-02.png)</a>